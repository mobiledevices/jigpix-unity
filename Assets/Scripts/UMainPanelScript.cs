﻿using UnityEngine;

public class UMainPanelScript : MonoBehaviour
{
    //Prefabs//
    public GameObject SettingsPanel;
    public GameObject InfoPanel;
    public GameObject PickYourPuzzlePanel;
    public GameObject EnterCodePanel;
    public GameObject YourPuzzlePanel;
    public GameObject QuestionPanel;
    public GameObject TakePhotoPanel;
    public GameObject SelectPhotoPanel;
    public GameObject ConvertPanel;
    public GameObject ResultPanel;
    public GameObject CodePanel;
    //END Prefabs//

    private void Start()
    {
        umSetPanel(UPanelType.PickYourPuzzle);
    }

    public GameObject umSetPanel(UPanelType panelType, UBasePanelScript prevPanel = null)
    {
        GameObject lvView = null;

        GameObject lvCurrentSetPanel = umGetPanelByType(panelType);
        if (lvCurrentSetPanel)
        {
            lvView = Instantiate(lvCurrentSetPanel);
            if (panelType == UPanelType.Info)
                lvView.transform.SetParent(GetComponentInParent<Canvas>().transform, false);
            else
                lvView.transform.SetParent(gameObject.transform, false);
            UBasePanelScript lvUBasePanelScript = lvView.GetComponent<UBasePanelScript>();
            if (lvUBasePanelScript)
            {
                lvUBasePanelScript.upPanelType = panelType;
                lvUBasePanelScript.upPrevPanel = prevPanel;
                lvUBasePanelScript.umMakeTransition();
            }
        }

        return lvView;
    }

    private GameObject umGetPanelByType(UPanelType panelType)
    {
        GameObject lvResult = null;

        switch (panelType)
        {
            case UPanelType.Settings:
                lvResult = SettingsPanel;
                break;
            case UPanelType.Info:
                lvResult = InfoPanel;
                break;
            case UPanelType.PickYourPuzzle:
                lvResult = PickYourPuzzlePanel;
                break;
            //case UPanelType.EnterCode:
            //    lvResult = EnterCodePanel;
            //    break;
            case UPanelType.YourPuzzle:
                lvResult = YourPuzzlePanel;
                break;
            case UPanelType.Question:
                lvResult = QuestionPanel;
                break;
            case UPanelType.TakePhoto:
                lvResult = TakePhotoPanel;
                break;
            case UPanelType.SelectPhoto:
                lvResult = SelectPhotoPanel;
                break;
            case UPanelType.Convert:
                lvResult = ConvertPanel;
                break;
            case UPanelType.Result:
                lvResult = ResultPanel;
                break;
            case UPanelType.Code:
                lvResult = CodePanel;
                break;
            default:
                break;
        }

        return lvResult;
    }
}

