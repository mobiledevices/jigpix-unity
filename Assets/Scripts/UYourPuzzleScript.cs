﻿using UnityEngine;
using UnityEngine.UI;

public class UYourPuzzleScript : UBasePanelScript
{
    public Image PuzzleImage;

    public override UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.Question;
        }
    }

    public override void Start()
    {
        base.Start();

        if (PuzzleImage)
        {
            UMainData lvMainData = UMainScript.upInstance.upMainData;
            string[] lvPuzzleArray = lvMainData.upPuzzleArray;
            string lvPuzzleItem = lvPuzzleArray[lvMainData.upSelectedPuzzleIndex];
            Object lvImageObject = Resources.Load(lvPuzzleItem);
            Texture2D lvTexture2D = lvImageObject as Texture2D;
            Sprite lvSprite = Sprite.Create(lvTexture2D, new Rect(0, 0, lvTexture2D.width, lvTexture2D.height), new Vector2(0.5f, 0.0f), 1.0f);
            PuzzleImage.sprite = lvSprite;
        }
    }
}
