﻿using UnityEngine;
using UnityEngine.UI;

public class UResultScript : UBasePanelScript
{
    public Button OriginalButton;
    public Button SwapButton;
    public Button PuzzleButton;

    public RawImage ConvertedImage;

    private Color m_PuzzleButtonColor;
    private Color m_PuzzleButtonTextColor;

    private bool m_IsPuzzle = true;

    public bool upIsPuzzle
    {
        set
        {
            if (m_IsPuzzle != value)
            {
                m_IsPuzzle = value;
                umOnPuzzleFill();
            }
        }
    }

    public override UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.Code;
        }
    }

    private Texture2D upConvertedImageTexture
    {
        get
        {
            return ConvertedImage.texture as Texture2D;
        }
        set
        {
            if (ConvertedImage.texture != null)
            {
                Texture2D lvTexture2D = ConvertedImage.texture as Texture2D;
                lvTexture2D.umDestroyTexture();
            }

            ConvertedImage.texture = value;
        }
    }

    public override void Start()
    {
        base.Start();

        if (OriginalButton)
        {
            OriginalButton.onClick.AddListener(umOnOriginalButtonClick);
        }
        if (SwapButton)
        {
            SwapButton.onClick.AddListener(umOnSwapButtonClick);
        }
        if (PuzzleButton)
        {
            PuzzleButton.onClick.AddListener(umOnPuzzleButtonClick);
            m_PuzzleButtonColor = PuzzleButton.GetComponent<Image>().color;
            m_PuzzleButtonTextColor = PuzzleButton.GetComponentInChildren<Text>().color;
        }

        umOnPuzzleFill();

        if (UMainScript.upInstance.upMainData.upSettingsData.upAutoSavePuzzleArtwork)
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                IOSCamera.Instance.SaveTextureToCameraRoll(upConvertedImageTexture);
            }
            else if (Application.platform == RuntimePlatform.Android)
            {
                AndroidCamera.Instance.SaveImageToGallery(upConvertedImageTexture);
            }
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (OriginalButton)
        {
            OriginalButton.onClick.RemoveListener(umOnOriginalButtonClick);
        }
        if (SwapButton)
        {
            SwapButton.onClick.RemoveListener(umOnSwapButtonClick);
        }
        if (PuzzleButton)
        {
            PuzzleButton.onClick.RemoveListener(umOnPuzzleButtonClick);
        }
    }

    private void umOnPuzzleButtonClick()
    {
        upIsPuzzle = true;
    }

    private void umOnSwapButtonClick()
    {
        upIsPuzzle = !m_IsPuzzle;
    }

    private void umOnOriginalButtonClick()
    {
        upIsPuzzle = false;
    }

    private void umOnPuzzleFill()
    {
        if (m_IsPuzzle)
        {
            OriginalButton.GetComponentInChildren<Text>().color = m_PuzzleButtonColor;
            OriginalButton.GetComponent<Image>().color = m_PuzzleButtonTextColor;

            PuzzleButton.GetComponentInChildren<Text>().color = m_PuzzleButtonTextColor;
            PuzzleButton.GetComponent<Image>().color = m_PuzzleButtonColor;

            Texture2D lvTexture2D = UMainScript.upInstance.upMainData.upConvertedRawData.umCreateTexture();
            upConvertedImageTexture = lvTexture2D;
        }
        else
        {
            OriginalButton.GetComponentInChildren<Text>().color = m_PuzzleButtonTextColor;
            OriginalButton.GetComponent<Image>().color = m_PuzzleButtonColor;

            PuzzleButton.GetComponentInChildren<Text>().color = m_PuzzleButtonColor;
            PuzzleButton.GetComponent<Image>().color = m_PuzzleButtonTextColor;

            Texture2D lvTexture2D = UMainScript.upInstance.upMainData.upSelectedPhotoRawData.umCreateTexture();
            upConvertedImageTexture = lvTexture2D;
        }
    }
}
