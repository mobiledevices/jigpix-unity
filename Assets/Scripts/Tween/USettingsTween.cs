﻿using UnityEngine;
using DigitalRuby.Tween;
using UnityEngine.EventSystems;
using System;

public class USettingsTween : MonoBehaviour, IPointerClickHandler
{
    public GameObject SettingsPanel;
    public GameObject BlockPanel;

    private const float cLeftSize = 120.0f;
    private const float cAnimationTime = 0.3f;

    private bool m_IsSettings = false;

    public void umShowSettings()
    {
        if (m_IsSettings)
        {
            umHideSettings();
        }
        else
        {
            m_IsSettings = true;

            if (SettingsPanel)
            {
                SettingsPanel.SetActive(true);
            }

            RectTransform lvRectTransform = GetComponent<RectTransform>();
            Vector3 lvStartPos = lvRectTransform.localPosition;
            float lvX = lvStartPos.x - lvRectTransform.rect.width + cLeftSize;
            Vector3 lvEndPos = new Vector3(lvX, lvStartPos.y, lvStartPos.z);
            //LTDescr d = LeanTween.move(lvRectTransform, lvEndPos, cAnimationTime).setEaseInQuad();
            //d.setOnComplete((t) =>
            //{
            //    // complete
            //    //this.gameObject.transform.localPosition = t.CurrentValue;
            //    if (BlockPanel)
            //    {
            //        BlockPanel.SetActive(true);
            //        BlockPanel.transform.SetAsLastSibling();
            //    }
            //});
            this.gameObject.Tween("MovePanel", lvStartPos, lvEndPos, cAnimationTime, TweenScaleFunctions.CubicEaseIn, (t) =>
            {
                // progress
                this.gameObject.transform.localPosition = t.CurrentValue;
            }, (t) =>
            {
                // complete
                this.gameObject.transform.localPosition = t.CurrentValue;
                if (BlockPanel)
                {
                    BlockPanel.SetActive(true);
                    BlockPanel.transform.SetAsLastSibling();
                }
            }
            );
        }
    }

    private void umHideSettings()
    {
        m_IsSettings = false;

        RectTransform lvRectTransform = GetComponent<RectTransform>();
        Vector3 lvStartPos = lvRectTransform.localPosition;
        float lvX = 0;
        Vector3 lvEndPos = new Vector3(lvX, lvStartPos.y, lvStartPos.z);
        this.gameObject.Tween("MovePanel", lvStartPos, lvEndPos, cAnimationTime, TweenScaleFunctions.CubicEaseIn, (t) =>
        {
            // progress
            this.gameObject.transform.localPosition = t.CurrentValue;
        }, (t) =>
        {
            // complete
            this.gameObject.transform.localPosition = t.CurrentValue;
            if (BlockPanel)
            {
                BlockPanel.SetActive(false);
                BlockPanel.transform.SetAsFirstSibling();
            }
            if (SettingsPanel)
            {
                SettingsPanel.SetActive(false);
            }
        }
        );
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        umHideSettings();
    }
}

