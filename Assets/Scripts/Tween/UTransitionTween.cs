﻿using UnityEngine;
using DigitalRuby.Tween;

public static class UTransitionTween
{
    private const float cAnimationTime = 0.2f;

    public static void umRightTransition(GameObject animateObj, GameObject prevObj)
    {
        RectTransform lvRectTransform = (RectTransform)animateObj.transform;
        Vector3 lvLocalPosition = animateObj.transform.localPosition;
        lvLocalPosition.x = lvRectTransform.rect.width;
        animateObj.transform.localPosition = lvLocalPosition;

        Vector3 lvStartPos = lvRectTransform.localPosition;
        float lvX = 0;
        Vector3 lvEndPos = new Vector3(lvX, lvStartPos.y, lvStartPos.z);
        animateObj.Tween("MovePanel", lvStartPos, lvEndPos, cAnimationTime, TweenScaleFunctions.Linear, (t) =>
        {
            // progress
            animateObj.transform.localPosition = t.CurrentValue;
        }, (t) =>
        {
            // complete
            animateObj.transform.localPosition = t.CurrentValue;
            if(prevObj)
                prevObj.SetActive(false);

            PinchZoom lvPinchZoom = animateObj.GetComponentInChildren<PinchZoom>();
            if (lvPinchZoom)
                lvPinchZoom.SetOriginalPosition();
            UZoomImagePanelScript lvUZoomImagePanelScript = animateObj.GetComponentInChildren<UZoomImagePanelScript>();
            if (lvUZoomImagePanelScript)
                lvUZoomImagePanelScript.umSetOriginalPosition();
        }
        );
    }

    public static void umRightBackTransition(GameObject animateObj)
    {
        RectTransform lvRectTransform = (RectTransform)animateObj.transform;
        Vector3 lvStartPos = lvRectTransform.localPosition;
        float lvX = lvRectTransform.rect.width;
        Vector3 lvEndPos = new Vector3(lvX, lvStartPos.y, lvStartPos.z);
        animateObj.Tween("MovePanel", lvStartPos, lvEndPos, cAnimationTime, TweenScaleFunctions.Linear, (t) =>
        {
            // progress
            animateObj.transform.localPosition = t.CurrentValue;
        }, (t) =>
        {
            // complete
            animateObj.transform.localPosition = t.CurrentValue;
            GameObject.DestroyImmediate(animateObj);
        }
        );
    }

    public static void umDownTransition(GameObject animateObj)
    {
        RectTransform lvRectTransform = (RectTransform)animateObj.transform;
        Vector3 lvLocalPosition = animateObj.transform.localPosition;
        lvLocalPosition.y = -lvRectTransform.rect.height;
        animateObj.transform.localPosition = lvLocalPosition;

        Vector3 lvStartPos = lvRectTransform.localPosition;
        float lvY = 0;
        Vector3 lvEndPos = new Vector3(lvStartPos.x, lvY, lvStartPos.z);
        animateObj.Tween("MovePanel", lvStartPos, lvEndPos, cAnimationTime, TweenScaleFunctions.Linear, (t) =>
        {
            // progress
            animateObj.transform.localPosition = t.CurrentValue;
        }, (t) =>
        {
            // complete
            animateObj.transform.localPosition = t.CurrentValue;
        }
        );
    }

    public static void umDownBackTransition(GameObject animateObj)
    {
        RectTransform lvRectTransform = (RectTransform)animateObj.transform;
        Vector3 lvStartPos = lvRectTransform.localPosition;
        float lvY = -lvRectTransform.rect.height;
        Vector3 lvEndPos = new Vector3(lvStartPos.x, lvY, lvStartPos.z);
        animateObj.Tween("MovePanel", lvStartPos, lvEndPos, cAnimationTime, TweenScaleFunctions.Linear, (t) =>
        {
            // progress
            animateObj.transform.localPosition = t.CurrentValue;
        }, (t) =>
        {
            // complete
            animateObj.transform.localPosition = t.CurrentValue;
            GameObject.DestroyImmediate(animateObj);
        }
        );
    }
}
