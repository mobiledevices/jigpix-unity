﻿using UnityEngine;
using UnityEngine.UI;

public class SwitchBehaviourScript : MonoBehaviour
{
    public Toggle Switch;
    public Image SwitchOnImage;
    public Image SwitchOffImage;

    // Use this for initialization
    void Start()
    {
        if (Switch)
        {
            Switch.onValueChanged.AddListener(umSwitchValueChanged);
            Switch.onValueChanged.Invoke(Switch.isOn);
        }
    }

    void OnDestroy()
    {
        if (Switch)
        {
            Switch.onValueChanged.RemoveListener(umSwitchValueChanged);
        }
    }

    private void umSwitchValueChanged(bool switched)
    {
        if (switched)
        {
            if (SwitchOnImage)
                SwitchOnImage.gameObject.SetActive(true);
            if (SwitchOffImage)
                SwitchOffImage.gameObject.SetActive(false);
        }
        else
        {
            if (SwitchOnImage)
                SwitchOnImage.gameObject.SetActive(false);
            if (SwitchOffImage)
                SwitchOffImage.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}