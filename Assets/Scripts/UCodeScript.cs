﻿using System;
using SA.Common.Models;
using UnityEngine;
using UnityEngine.UI;

public class UCodeScript : UBasePanelScript
{
    #region Fields

    public Button PuzzleButton;
    public Button SwapButton;
    public Button EmojiButton;

    public RawImage CodeImage;

    public Button InstagramButton;
    public Button FacebookButton;
    public Button SaveButton;
    public Button ShareButton;

    private Color m_PuzzleButtonColor;
    private Color m_PuzzleButtonTextColor;

    private bool m_IsPuzzle = false;

    #endregion

    #region Const

    private const string cScreenshotTitle = "Jigpix Screenshot";

    #endregion

    #region Property

    public bool upIsPuzzle
    {
        set
        {
            if (m_IsPuzzle != value)
            {
                m_IsPuzzle = value;
                umOnPuzzleFill();
            }
        }
    }

    private Texture2D upCodeImageTexture
    {
        get
        {
            return CodeImage.texture as Texture2D;
        }
        set
        {
            if (CodeImage.texture != null)
            {
                Texture2D lvTexture2D = CodeImage.texture as Texture2D;
                lvTexture2D.umDestroyTexture();
            }

            CodeImage.texture = value;
        }
    }

    #endregion

    #region Overrides

    public override void Start()
    {
        base.Start();

        if (EmojiButton)
        {
            EmojiButton.onClick.AddListener(umOnEmojiButtonClick);
        }
        if (SwapButton)
        {
            SwapButton.onClick.AddListener(umOnSwapButtonClick);
        }
        if (PuzzleButton)
        {
            PuzzleButton.onClick.AddListener(umOnPuzzleButtonClick);
            m_PuzzleButtonColor = PuzzleButton.GetComponent<Image>().color;
            m_PuzzleButtonTextColor = PuzzleButton.GetComponentInChildren<Text>().color;
        }
        if (InstagramButton)
        {
            InstagramButton.onClick.AddListener(umOnInstagramButtonClick);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                IOSSocialManager.OnInstagramPostResult += umIOSSocialManager_OnSocialPostResult;
            }
        }
        if (FacebookButton)
        {
            FacebookButton.onClick.AddListener(umOnFacebookButtonClick);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                IOSSocialManager.OnFacebookPostResult += umIOSSocialManager_OnSocialPostResult;
            }
        }
        if (SaveButton)
        {
            SaveButton.onClick.AddListener(umOnSaveButtonClick);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                IOSCamera.OnImageSaved += umIOSCamera_OnImageSaved;
            }
            else if (Application.platform == RuntimePlatform.Android)
            {
                AndroidCamera.Instance.OnImageSaved += umAndroidCamera_OnImageSaved;
            }
        }
        if (ShareButton)
        {
            ShareButton.onClick.AddListener(umOnShareButtonClick);
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidSocialGate.OnShareIntentCallback += umHandleOnShareIntentCallback;
        }

        umOnPuzzleFill();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (EmojiButton)
        {
            EmojiButton.onClick.RemoveListener(umOnEmojiButtonClick);
        }
        if (SwapButton)
        {
            SwapButton.onClick.RemoveListener(umOnSwapButtonClick);
        }
        if (PuzzleButton)
        {
            PuzzleButton.onClick.RemoveListener(umOnPuzzleButtonClick);
        }
        if (InstagramButton)
        {
            InstagramButton.onClick.RemoveListener(umOnInstagramButtonClick);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                IOSSocialManager.OnInstagramPostResult -= umIOSSocialManager_OnSocialPostResult;
            }
        }
        if (FacebookButton)
        {
            FacebookButton.onClick.RemoveListener(umOnFacebookButtonClick);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                IOSSocialManager.OnFacebookPostResult -= umIOSSocialManager_OnSocialPostResult;
            }
        }
        if (SaveButton)
        {
            SaveButton.onClick.RemoveListener(umOnSaveButtonClick);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                IOSCamera.OnImageSaved -= umIOSCamera_OnImageSaved;
            }
            else if (Application.platform == RuntimePlatform.Android)
            {
                AndroidCamera.Instance.OnImageSaved -= umAndroidCamera_OnImageSaved;
            }
        }
        if (ShareButton)
        {
            ShareButton.onClick.RemoveListener(umOnShareButtonClick);
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidSocialGate.OnShareIntentCallback -= umHandleOnShareIntentCallback;
        }
    }

    #endregion

    #region Swap Panel

    private void umOnPuzzleButtonClick()
    {
        upIsPuzzle = true;
    }

    private void umOnSwapButtonClick()
    {
        upIsPuzzle = !m_IsPuzzle;
    }

    private void umOnEmojiButtonClick()
    {
        upIsPuzzle = false;
    }

    private void umOnPuzzleFill()
    {
        if (m_IsPuzzle)
        {
            EmojiButton.GetComponentInChildren<Text>().color = m_PuzzleButtonColor;
            EmojiButton.GetComponent<Image>().color = m_PuzzleButtonTextColor;

            PuzzleButton.GetComponentInChildren<Text>().color = m_PuzzleButtonTextColor;
            PuzzleButton.GetComponent<Image>().color = m_PuzzleButtonColor;

            Texture2D lvTexture2D = UMainScript.upInstance.upMainData.upConvertedRawData.umCreateTexture();
            upCodeImageTexture = lvTexture2D;
        }
        else
        {
            EmojiButton.GetComponentInChildren<Text>().color = m_PuzzleButtonTextColor;
            EmojiButton.GetComponent<Image>().color = m_PuzzleButtonColor;

            PuzzleButton.GetComponentInChildren<Text>().color = m_PuzzleButtonColor;
            PuzzleButton.GetComponent<Image>().color = m_PuzzleButtonTextColor;

            Texture2D lvTexture2D = UMainScript.upInstance.upMainData.upCodeRawData.umCreateTexture();
            upCodeImageTexture = lvTexture2D;
        }
    }

    #endregion

    #region Social

    private void umOnInstagramButtonClick()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            AndroidSocialGate.StartShareIntent(cScreenshotTitle, null, lvTexture2D, "insta");
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            IOSSocialManager.Instance.InstagramPost(lvTexture2D, cScreenshotTitle);
        }
    }

    private void umOnFacebookButtonClick()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            AndroidSocialGate.StartShareIntent(cScreenshotTitle, null, lvTexture2D, "facebook.katana");
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            IOSSocialManager.Instance.FacebookPost(cScreenshotTitle, null, lvTexture2D);
        }
    }

    private void umIOSSocialManager_OnSocialPostResult(SA.Common.Models.Result result)
    {
        if (result.IsSucceeded)
        {
            IOSNativePopUpManager.showMessage("Posting Jigpix", "Post Success!");
        }
        else if(result.IsFailed)
        {
            IOSNativePopUpManager.showMessage("Posting Jigpix", "Post Failed :( Errod code: " + result.Error.Code);
        }
    }

    #endregion

    #region Save

    private void umOnSaveButtonClick()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            AndroidCamera.Instance.SaveImageToGallery(lvTexture2D);
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            IOSCamera.Instance.SaveTextureToCameraRoll(lvTexture2D);
        }
    }

    private void umIOSCamera_OnImageSaved(SA.Common.Models.Result result)
    {
        if (result.IsSucceeded)
        {
            IOSMessage.Create("Success", "Image Successfully saved to Camera Roll");
        }
        else if(result.IsFailed)
        {
            IOSMessage.Create("Failed", "Image Save Failed");
        }
    }

    private void umAndroidCamera_OnImageSaved(GallerySaveResult obj)
    {
        if (obj.IsSucceeded)
        {
            AndroidToast.ShowToastNotification("Image Successfully saved to Camera Roll");
        }
        else if(obj.IsFailed)
        {
            AndroidToast.ShowToastNotification("Image Save Failed");
        }
    }

    #endregion

    #region Share

    private void umOnShareButtonClick()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            AndroidSocialGate.StartShareIntent(cScreenshotTitle, null, lvTexture2D);
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Texture2D lvTexture2D = upCodeImageTexture;
            IOSSocialManager.Instance.ShareMedia(cScreenshotTitle, lvTexture2D);
        }
    }

    private void umHandleOnShareIntentCallback(bool status, string package)
    {
        //AndroidToast.ShowToastNotification((status ? "Success" : "Failed") + " " + package);
    }

    #endregion
}
