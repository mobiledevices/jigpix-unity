﻿using System.Collections;
using UnityEngine;


/// <summary>
/// FPSDisplayScript 
/// </summary>
public class VHelperStatisticFPS : MonoBehaviour
{
    private int FramesPerSec;
    private float frequency = 1.0f;
    private string fps;

    //float timeA;
    //public int fps;
    //public int lastFPS;

    public GUIStyle textStyle;
    // Use this for initialization
    void Start()
    {
        //timeA = Time.timeSinceLevelLoad;
        //DontDestroyOnLoad(this);
        StartCoroutine(FPS());
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Time.timeSinceLevelLoad - timeA <= 1)
        {
            fps++;
        }
        else
        {
            lastFPS = fps;
            timeA = Time.timeSinceLevelLoad;
            fps = 0;
        }*/
    }

    private IEnumerator FPS()
    {
        for (;;)
        {
            // Capture frame-per-second
            int lastFrameCount = Time.frameCount;
            float lastTime = Time.realtimeSinceStartup;
            yield return new WaitForSeconds(frequency);
            float timeSpan = Time.realtimeSinceStartup - lastTime;
            int frameCount = Time.frameCount - lastFrameCount;

            // Display it

            fps = string.Format("FPS: {0}", Mathf.RoundToInt(frameCount / timeSpan));
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(5, 5, 30, 30), fps, textStyle);
    }
}