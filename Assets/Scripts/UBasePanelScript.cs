﻿using UnityEngine;
using UnityEngine.UI;

public class UBasePanelScript : MonoBehaviour
{
    #region Fields

    public GameObject TopPanel;
    public Button NextButton;

    protected Button m_BackButton;
    private Button m_SettingsButton;

    private UPanelType m_PanelType = UPanelType.Unknown;

    protected RectTransform m_RectTransform;

    private UBasePanelScript m_PrevPanel;

    #endregion

    #region Property

    public virtual UPanelType upPanelType
    {
        get
        {
            return m_PanelType;
        }
        set
        {
            if (m_PanelType == UPanelType.Unknown)
                m_PanelType = value;
        }
    }

    public virtual UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.Unknown;
        }
    }

    public virtual UBasePanelScript upPrevPanel
    {
        set
        {
            m_PrevPanel = value;
        }
    }

    public virtual bool upAnimate
    {
        get
        {
            return true;
        }
    }



    #endregion

    #region MonoBehaviour

    public virtual void Start()
    {
        m_RectTransform = (RectTransform)transform;

        if (TopPanel)
        {
            Transform lvBackButtonTransform = TopPanel.transform.Find("BackButton");
            if (lvBackButtonTransform)
            {
                m_BackButton = lvBackButtonTransform.GetComponent<Button>();
                if (m_BackButton)
                {
                    m_BackButton.onClick.AddListener(umOnBackButtonClick);
                }
            }

            Transform lvSettingsButtonTransform = TopPanel.transform.Find("SettingsButton");
            if (lvSettingsButtonTransform)
            {
                m_SettingsButton = lvSettingsButtonTransform.GetComponent<Button>();
                if (m_SettingsButton)
                {
                    m_SettingsButton.onClick.AddListener(umOnSettingsButtonClick);
                }
            }
        }
        if (NextButton)
            NextButton.onClick.AddListener(umOnNextButtonClick);
    }

    public virtual void OnEnable()
    {

    }

    public virtual void OnDisable()
    {

    }

    public virtual void Update()
    {
    }

    public virtual void OnDestroy()
    {
        if (m_BackButton)
            m_BackButton.onClick.RemoveListener(umOnBackButtonClick);
        if (m_SettingsButton)
            m_SettingsButton.onClick.RemoveListener(umOnSettingsButtonClick);
        if (NextButton)
            NextButton.onClick.AddListener(umOnNextButtonClick);
    }

    #endregion

    #region Methods

    public void umMakeTransition()
    {
        if (upAnimate)
        {
            UTransitionTween.umRightTransition(this.gameObject, m_PrevPanel ? m_PrevPanel.gameObject : null);
        }
    }

    protected virtual void umOnBackButtonClick()
    {
        if (m_PrevPanel)
            m_PrevPanel.gameObject.SetActive(true);

        if (upAnimate)
        {
            UTransitionTween.umRightBackTransition(this.gameObject);
        }
    }

    protected void umOnSettingsButtonClick()
    {
        //GameObject lvSettingsGameObject = UMainScript.upInstance.umSetPanel(UPanelType.Settings, this);

        USettingsTween lvSettingsTween = UMainScript.upInstance.MainPanel.GetComponent<USettingsTween>();
        if (lvSettingsTween)
        {
            lvSettingsTween.umShowSettings();
        }
    }

    protected virtual void umOnNextButtonClick()
    {
        UMainScript.upInstance.umSetPanel(upNextPanelType, this);
    }

    #endregion
}
