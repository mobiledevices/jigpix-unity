﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class USelectPhotoScript : UBasePanelScript
{
    public GridLayoutGroup GridLayoutGroup;
    public Button ImageButtonTemplate;
    public Button ImageButtonPlus;
    public Image SelectedImage;

    public override UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.Convert;
        }
    }

    public override void Start()
    {
        base.Start();
    }

    public override void OnEnable()
    {
        base.OnEnable();

        if (ImageButtonPlus)
        {
            ImageButtonPlus.onClick.AddListener(umOnImageButtonPlusClick);
        }

        umInitFromGallery();
    }

    private void umInitFromGallery()
    {
        string[] lvImagesArray = null;
        //if (Application.platform == RuntimePlatform.Android)
        //    lvImagesArray = umGetAllGalleryImagePathsAndroid();
        //else
        lvImagesArray = umGetAllGalleryImagePaths();

        for (int i = 0; i < lvImagesArray.Length; i++)
        {
            if (GridLayoutGroup)
            {
                if (ImageButtonTemplate)
                {
                    Button lvImageButton = Button.Instantiate(ImageButtonTemplate, this.GridLayoutGroup.transform) as Button;
                    lvImageButton.name = "ImageButton" + (i + 1).ToString();
                    lvImageButton.gameObject.SetActive(true);
                    lvImageButton.onClick.AddListener(delegate { umOnImageButtonClick(lvImageButton); });
                    UImageButtonVisible lvUImageButtonVisible = lvImageButton.GetComponent<UImageButtonVisible>();
                    if (lvUImageButtonVisible)
                    {
                        lvUImageButtonVisible.upLoadPath = lvImagesArray[i];
                    }
                }
            }
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();

        if (GridLayoutGroup)
        {
            foreach (UImageButtonVisible lvButton in GridLayoutGroup.GetComponentsInChildren<UImageButtonVisible>(false))
            {
                GameObject.Destroy(lvButton.gameObject);
            }
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (GridLayoutGroup)
        {
            Button[] lvImageButtons = GridLayoutGroup.GetComponentsInChildren<Button>();

            foreach (Button lvImageButton in lvImageButtons)
            {
                if (ImageButtonPlus != lvImageButton)
                {
                    lvImageButton.onClick.RemoveListener(delegate { umOnImageButtonClick(null); });
                }
            }

            if (ImageButtonPlus)
            {
                ImageButtonPlus.onClick.RemoveListener(umOnImageButtonPlusClick);
            }
        }
    }

    private void umOnImageButtonPlusClick()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            IOSCamera.OnImagePicked += umOnImage;
            IOSCamera.Instance.PickImage(ISN_ImageSource.Library);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            AndroidCamera.Instance.OnImagePicked += umOnImagePicked;
            AndroidCamera.Instance.GetImageFromGallery();
        }
    }

    private void umOnImage(IOSImagePickResult result)
    {
        IOSCamera.OnImagePicked -= umOnImage;
        if (result.IsSucceeded)
        {
            Texture2D lvTexture2D = result.Image;
            Sprite lvSprite = Sprite.Create(lvTexture2D, new Rect(0, 0, lvTexture2D.width, lvTexture2D.height), new Vector2(0.5f, 0.0f), 1.0f);
            SelectedImage.sprite = lvSprite;
        }
        else if (result.IsFailed)
        {
            IOSNativePopUpManager.showMessage("Image Pick Result", "Failed!");
        }
    }

    private void umOnImagePicked(AndroidImagePickResult result)
    {
        AndroidCamera.Instance.OnImagePicked -= umOnImagePicked;

        if (result.IsSucceeded)
        {
            //AN_PoupsProxy.showMessage("Image Pick Result", "Succeeded, path: " + result.ImagePath);
            Texture2D lvTexture2D = result.Image;
            Sprite lvSprite = Sprite.Create(lvTexture2D, new Rect(0, 0, lvTexture2D.width, lvTexture2D.height), new Vector2(0.5f, 0.0f), 1.0f);
            SelectedImage.sprite = lvSprite;
        }
        else if (result.IsFailed)
        {
            AN_PoupsProxy.showMessage("Image Pick Result", "Failed");
        }
    }

    private void umOnImageButtonClick(Button sender)
    {
        if (sender && SelectedImage)
        {
            UImageButtonVisible lvUImageButtonVisible = sender.GetComponent<UImageButtonVisible>();
            if (lvUImageButtonVisible)
            {
                Texture2D lvTexture2D = lvUImageButtonVisible.upTexture;
                Sprite lvSprite = Sprite.Create(lvTexture2D, new Rect(0, 0, lvTexture2D.width, lvTexture2D.height), new Vector2(0.5f, 0.0f), 1.0f);
                SelectedImage.sprite = lvSprite;
            }
        }
    }

    protected override void umOnNextButtonClick()
    {
        if (SelectedImage && SelectedImage.sprite)
        {
            UMainScript.upInstance.upMainData.upSelectedPhotoRawData = SelectedImage.sprite.texture.EncodeToPNG();
            base.umOnNextButtonClick();
        }
    }

    private string[] umGetAllGalleryImagePaths()
    {
        return UMainScript.upInstance.upMainData.upPuzzleArray;
    }


    private string[] umGetAllGalleryImagePathsAndroid()
    {
        List<string> results = new List<string>();
        HashSet<string> allowedExtesions = new HashSet<string>() { ".png", ".jpg", ".jpeg" };

        try
        {
            AndroidJavaClass mediaClass = new AndroidJavaClass("android.provider.MediaStore$Images$Media");

            // Set the tags for the data we want about each image.  This should really be done by calling; 
            //string dataTag = mediaClass.GetStatic<string>("DATA");
            // but I couldn't get that to work...

            const string dataTag = "_data";

            string[] projection = new string[] { dataTag };
            AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = player.GetStatic<AndroidJavaObject>("currentActivity");

            string[] urisToSearch = new string[] { "EXTERNAL_CONTENT_URI", "INTERNAL_CONTENT_URI" };
            foreach (string uriToSearch in urisToSearch)
            {
                AndroidJavaObject externalUri = mediaClass.GetStatic<AndroidJavaObject>(uriToSearch);
                AndroidJavaObject finder = currentActivity.Call<AndroidJavaObject>("managedQuery", externalUri, projection, null, null, null);
                bool foundOne = finder.Call<bool>("moveToFirst");
                while (foundOne)
                {
                    int dataIndex = finder.Call<int>("getColumnIndex", dataTag);
                    string data = finder.Call<string>("getString", dataIndex);
                    if (allowedExtesions.Contains(System.IO.Path.GetExtension(data).ToLower()))
                    {
                        string path = @"file:///" + data;
                        results.Add(path);
                    }

                    foundOne = finder.Call<bool>("moveToNext");
                }
            }
        }
        catch (System.Exception exc)
        {
            ShowToast.showToastOnUiThread(exc.Message);
        }

        return results.ToArray();
    }

}
