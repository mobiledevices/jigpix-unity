﻿using UnityEngine;
using UnityEngine.UI;

public class UEnterCodeScript : UBasePanelScript
{

    public Button BackButton;
    public InputField CodeInputField;

    private Color m_ContinueButtonColor;
    private Color m_ContinueButtonTextColor;

    private const string cDefaultCode = "1234-ABCD";

    public override UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.YourPuzzle;
        }
    }

    public override void Start()
    {
        base.Start();

        if (BackButton)
        {
            BackButton.onClick.AddListener(umOnBackButtonClick);
        }
        if (CodeInputField)
        {
            CodeInputField.characterLimit = cDefaultCode.Length;
            CodeInputField.onValueChanged.AddListener(umOnCodeValueChanged);
            CodeInputField.onValidateInput += umOnValidateInput;
            CodeInputField.keyboardType = TouchScreenKeyboardType.ASCIICapable;
        }
        if (NextButton)
        {
            m_ContinueButtonColor = NextButton.GetComponent<Image>().color;
            m_ContinueButtonTextColor = NextButton.GetComponentInChildren<Text>().color;
        }
    }

    public override void OnEnable()
    {
        base.OnEnable();

        CodeInputField.text = string.Empty;
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (BackButton)
        {
            BackButton.onClick.RemoveListener(umOnBackButtonClick);
        }
        if (CodeInputField)
        {
            CodeInputField.onValueChanged.RemoveListener(umOnCodeValueChanged);
            CodeInputField.onValidateInput -= umOnValidateInput;
        }
    }

    private char umOnValidateInput(string text, int charIndex, char addedChar)
    {
        if (charIndex < 4)
        {
            if (!char.IsDigit(addedChar))
                addedChar = '\0';
        }
        else if (charIndex > 4)
        {
            if (!char.IsLetter(addedChar))
                addedChar = '\0';
        }

        return addedChar;
    }

    private string m_OldCodeValue = string.Empty;

    private void umOnCodeValueChanged(string value)
    {
        if (NextButton)
        {
            if (value.Length == 4)
            {
                if (m_OldCodeValue.Length > 4)
                {
                    CodeInputField.text = value.Substring(0, 3);
                }
                else
                {
                    CodeInputField.text = value += "-";
                }
                CodeInputField.caretPosition = CodeInputField.text.Length;
            }

            m_OldCodeValue = CodeInputField.text;

            if (value == cDefaultCode)
            {
                NextButton.GetComponentInChildren<Text>().color = m_ContinueButtonColor;
                NextButton.GetComponent<Image>().color = m_ContinueButtonTextColor;
            }
            else
            {
                NextButton.GetComponentInChildren<Text>().color = m_ContinueButtonTextColor;
                NextButton.GetComponent<Image>().color = m_ContinueButtonColor;
            }
        }
    }
}
