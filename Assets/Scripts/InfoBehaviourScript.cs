﻿using UnityEngine;
using UnityEngine.UI;

public class InfoBehaviourScript : MonoBehaviour
{
    public Button BackButton;
    public Text HeaderText;
    public Text BodyText;

    private void Start()
    {
        if (BackButton)
        {
            BackButton.onClick.AddListener(umOnBackButtonClick);
        }

        UTransitionTween.umDownTransition(this.gameObject);
    }

    private void OnDestroy()
    {
        if (BackButton)
        {
            BackButton.onClick.RemoveListener(umOnBackButtonClick);
        }
    }

    private void umOnBackButtonClick()
    {
        UTransitionTween.umDownBackTransition(this.gameObject);
    }
}

