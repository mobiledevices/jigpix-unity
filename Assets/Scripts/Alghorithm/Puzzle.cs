﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle
{
    public PuzzlePiece[] pieces;
    PuzzlePiece[] targetPieces;

    public int nPiecesHoriz;
    int nPiecesVert;
    public int nTotalPieces;
    public int smallResHoriz;
    int smallResVert;

    public double powerFunc = 0.0;
    public double centralImortance = 0.0;


    //--------------------------------------------------------------
    public void setup(int cols, int rows)
    {
        centralImortance = 1;
        powerFunc = 0.6;
        nPiecesHoriz = cols;//20;
        nPiecesVert = rows;//26;
        nTotalPieces = nPiecesHoriz * nPiecesVert;
        smallResHoriz = cols * PuzzlePiece.CHOPSIZE; // 300;
        smallResVert = rows * PuzzlePiece.CHOPSIZE; //390;

        // allocate the pieces
        this.pieces = new PuzzlePiece[nTotalPieces]; // Array(count: nTotalPieces, repeatedValue: PuzzlePiece());
        this.targetPieces = new PuzzlePiece[nTotalPieces]; // Array(count: nTotalPieces, repeatedValue: PuzzlePiece());
    }

    //--------------------------------------------------------------
    public void setFromImage(Texture2D inputImage, bool target)
    {
        Color32[,] resizedImage = inputImage.ScaleTextureColors(smallResHoriz, smallResVert); //PuzzlePiece.ScaleTexture(inputImage, smallResHoriz, smallResVert);
        int pieceSizeW = smallResHoriz / nPiecesHoriz;

        for (int p = 0; p < nTotalPieces; p++)
        {
            int row = p / nPiecesHoriz;
            int col = p % nPiecesHoriz;

            Rect cropRect = new Rect(col * pieceSizeW, row * pieceSizeW, pieceSizeW, pieceSizeW);
            Color32[,] cropImage = resizedImage.CropColorArray(cropRect); //Texture2D cropImage = PuzzlePiece.cropTexture(resizedImage, cropRect);

            if (target)
            {
                this.targetPieces[p] = new PuzzlePiece();
                this.targetPieces[p].id = p;
                this.targetPieces[p].calculateStatistics(cropImage, cols: nPiecesHoriz, rows: nPiecesVert);
            }
            else
            {
                this.pieces[p] = new PuzzlePiece();
                this.pieces[p].id = p;
                this.pieces[p].calculateStatistics(cropImage, cols: nPiecesHoriz, rows: nPiecesVert);
            }
        }
    }

    //--------------------------------------------------------------
    public bool tryToSwapStuff()
    {
        //var lvRandom = new System.Random();
        //int posA = lvRandom.Next(0, nTotalPieces);
        //int posB = lvRandom.Next(0, nTotalPieces);
        int posA = UnityEngine.Random.Range(0, nTotalPieces);
        int posB = UnityEngine.Random.Range(0, nTotalPieces);

        if (posA == posB)
        {
            return false;
        }

        double CAatTA = 0.0, CBatTB = 0.0;
        double CAatTB = 0.0, CBatTA = 0.0;
        int AatBrots = 0, BatArots = 0, notused = 0;

        var TAptr = targetPieces[posA];
        var TBptr = targetPieces[posB];
        var CAptr = pieces[posA];
        var CBptr = pieces[posB];

        /* calc mismatches */
        CAatTA = mismatch(CAptr, chopPtr: TAptr, bestRot: ref notused); /* CCWs already recorded  */
                                                                     /* will CAatTA *= (C_CENT * TAdis); */

        CBatTB = mismatch(CBptr, chopPtr: TBptr, bestRot: ref notused); /* CCWs already recorded  */
                                                                     /* will CBatTB *= (C_CENT * TBdis); */

        CAatTB = mismatch(CAptr, chopPtr: TBptr, bestRot: ref AatBrots); /* AatB, newCCWs if swapt */
                                                                      /* will CAatTB *= (C_CENT * TBdis); */

        CBatTA = mismatch(CBptr, chopPtr: TAptr, bestRot: ref BatArots); /* BatA, newCCWs if swapt */
                                                                      /* will CBatTA += (C_CENT * TAdis); */


        //	/* Central mismatch can be 1 to 9 times as important as at corner.
        //	 * Its adjusted value f(CentralImportance * DistToCorner) is thus
        //	 * about from 100% to 900% of originglly (for max import 1 thru 9),
        //	 * approximated by: newval = oldval + (oldval*(import-1)*distance)/10
        //	 * (Note two different distances are involved for locs A & B)
        //	 */

        double importminus = this.centralImortance;

        int loca = TAptr.id;
        int locb = TBptr.id;

        int COLS = this.nPiecesHoriz;

        int Acol = loca % COLS;
        int Arow = loca / COLS;
        int Bcol = locb % COLS;
        int Brow = locb / COLS;

        int ax = (Acol < 10 ? Acol : 19 - Acol);
        int ay = (Arow < 13 ? Arow : 25 - Arow);
        int bx = (Bcol < 10 ? Bcol : 19 - Bcol);
        int by = (Brow < 13 ? Brow : 25 - Brow);

        //        let Adist = (Acol<10 ? Acol : 19-Acol)+(Arow<13 ? Arow : 25-Arow);
        //        let Bdist = (Bcol<10 ? Bcol : 19-Bcol)+(Brow<13 ? Brow : 25-Brow);

        var distanceA = System.Math.Min(System.Math.Pow(System.Math.Sqrt((double)(ax * ax)) / System.Math.Sqrt(10 * 10), powerFunc), System.Math.Pow(System.Math.Sqrt((double)(ay * ay)) / System.Math.Sqrt(13 * 13), powerFunc));
        var distanceB = System.Math.Min(System.Math.Pow(System.Math.Sqrt((double)(bx * bx)) / System.Math.Sqrt(10 * 10), powerFunc), System.Math.Pow(System.Math.Sqrt((double)(by * by)) / System.Math.Sqrt(13 * 13), powerFunc));

        distanceA *= (importminus / 5.0);
        distanceB *= (importminus / 5.0);

        CAatTA += ((CAatTA * distanceA));
        CBatTB += ((CBatTB * distanceB));
        CAatTB += ((CAatTB * distanceB));
        CBatTA += ((CBatTA * distanceA));


        //print("mismatch %i %i \n", CAatTB + CBatTA, CAatTA + CBatTB);
        /* if chopup tiles best as they are, do not swap */
        if ((CAatTB + CBatTA) >= (CAatTA + CBatTB))
        {
            //print("we're not swapping \n");
            return false;
        }
        else
        {

        }
        /* Exchange 1. plant anticipated new miss data */
        pieces[posA].rotation = AatBrots;
        pieces[posB].rotation = BatArots;

        var temp = pieces[posA];
        pieces[posA] = pieces[posB];
        pieces[posB] = temp;

        return true;
    }

    //--------------------------------------------------------------
    public double mismatch(PuzzlePiece targPtr, PuzzlePiece chopPtr, ref int bestRot)
    {
        // ------------------------------------------------------------
        // find the best rotation
        // ------------------------------------------------------------

        int Crit = 0, Cbot = 0;  /* chopup tile */
        int Trit = 0, Tbot = 0;  /* target tile */
        double SumRitBotSq = 0.0, nowsum = 0.0;
        int newbot = 0;

        //        Cbri = chopPtr.bri;
        Crit = chopPtr.rit;
        Cbot = chopPtr.bot;
        //        Tbri = targPtr.bri;
        Trit = targPtr.rit;
        Tbot = targPtr.bot;

        SumRitBotSq = 999999999;   /* <-- will certainly be lowered */
        for (int i = 0; i < 4; i++)
        {
            double tmp = (double)((Crit - Trit) * (Crit - Trit) + (Cbot - Tbot) * (Cbot - Tbot));
            nowsum = System.Math.Sqrt(tmp);
            //cout << nowsum << endl;
            if (SumRitBotSq > nowsum)
            {
                SumRitBotSq = nowsum;
                bestRot = i;  /* best addt'l CCWs IFF swapped */
            }
            /* rotate chopup tile vals 90 degrees (CCW TEMP VALS ONLY) */
            newbot = -Crit;
            Crit = Cbot;
            Cbot = newbot;
        }


        // ------------------------------------------------------------
        // w/ the best rotation, find the per pixel difference of the smaller images
        // based on this metric http://www.compuphase.com/cmetric.htm
        // ------------------------------------------------------------

        double distance = 0.0;

        var pixelsA = chopPtr.colorImgPixData[bestRot]; //chopPtr.getPixels(chopPtr.colorImg[bestRot]!);
        var pixelsB = targPtr.colorImgPixData[0]; //targPtr.getPixels(targPtr.colorImg[0]!);

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                int rmean = (pixelsA[i, j].r + pixelsB[i, j].r) / 2;
                int r = pixelsA[i, j].r - pixelsB[i, j].r;
                int g = pixelsA[i, j].g - pixelsB[i, j].g;
                int b = pixelsA[i, j].b - pixelsB[i, j].b;
                double tmp = (double)((((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8));
                distance += System.Math.Sqrt(tmp);
            }
        }
        distance /= 25.0;

        return (distance * distance);
    }
}
