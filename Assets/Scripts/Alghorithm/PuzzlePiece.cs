﻿using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece
{
    public const int CHOPSIZE = 15;
    public const int TILEPIXELS = CHOPSIZE * CHOPSIZE;

    public Color32[,] img;

    public List<Color32[,]> colorImgPixData = new List<Color32[,]>();

    public int rotation = 0;                   //0 1 2 3 clockwise;

    int bri = 0;  /* average: (red+gre+blu)/3                         */
    int gre = 0;  /* average green                                    */
    int rmb = 0;  /* average red - average blue                       */
    public int rit = 0;  /* increase in (r+g+b)/3 center to mid right  edge  */
    public int bot = 0;  /* increase in (r+g+b)/3 center to mid bottom edge  */

    public int id = 0; // my ORIGINAL id.

    public override string ToString()
    {
        return string.Format("SN='{0}' BRI='{1}' GRE='{2}' RMB='{3}' RIT='{4}' BOT='{5}'", id, bri, gre, rmb, rit, bot);
    }

    public void processTile(Color32[,] pixels, PuzzlePiece tptr)
    {
        int red = 0, gre = 0, blu = 0, bri = 0;         /* vals per pixel 0-255 */
        int redsum = 0, gresum = 0, blusum = 0, brisum = 0;      /* sums of 15x15 pixels */
        int rit75 = 0, lef75 = 0, top75 = 0, bot75 = 0;       /* sums of 75 bri's     */
        int redavg = 0, greavg = 0, bluavg = 0, briavg = 0;

        /* array is 1-D, let's pretend its 2-D for the moment */
        for (int y = 0; y < CHOPSIZE; y++)
        {
            for (int x = 0; x < CHOPSIZE; x++)
            {
                /* note----> ++ sequences thru R,G,B, R,G,B values */
                red = pixels[y, x].r; redsum += red;
                gre = pixels[y, x].g; gresum += gre;
                blu = pixels[y, x].b; blusum += blu;
                bri = red + gre + blu; brisum += bri; /* 3x */

                /* sums of left & right & top & bot thirds */
                if (x < 5) { lef75 += bri; }
                if (x > 9) { rit75 += bri; }
                if (y < 5) { top75 += bri; }
                if (y > 9) { bot75 += bri; }
            }
        }

        redavg = redsum / TILEPIXELS;
        greavg = gresum / TILEPIXELS;
        bluavg = blusum / TILEPIXELS;
        briavg = brisum / (TILEPIXELS * 3);       /* was 3x */

        /* load results into TILE struct */
        tptr.bri = briavg;
        tptr.gre = greavg;
        tptr.rmb = redavg - bluavg;
        tptr.rit = ((rit75 - lef75) * 7) / 10;       /* WARNING: specific */
        tptr.bot = ((bot75 - top75) * 7) / 10;       /* for 15 x 15 tiles */

        //print(" tile info %i %i %i %i %i \n",  tptr.bri, tptr.gre,  tptr.rmb, tptr.rit, tptr.bot);

        /* note on (( )*7)/10 The centers of left and right sample areas are
         * 10 pixels apart; the right edge pixel is only 7 pixels right of
         * center, thus the factor 7/10. Likewise for top and bottom samples
         */

        //print("%i %i %i %i %i \n", tptr.bri, tptr.gre, tptr.rmb, tptr.rit, tptr.bot);
    }

    public void calculateStatistics(Color32[,] imagePiece, int cols, int rows)
    {
        this.rotation = 0;

        this.img = imagePiece;

        Texture2D lvResultTexture = new Texture2D(CHOPSIZE, CHOPSIZE);
        lvResultTexture.SetPixels(0, 0, CHOPSIZE, CHOPSIZE, imagePiece.ConvertTwoToOneDimensionArray());
        lvResultTexture.Apply();
        Color32[,] imagePieceSized = lvResultTexture.ScaleTextureColors(5, 5);

        this.colorImgPixData.Clear();
        this.colorImgPixData.Add(imagePieceSized);
        var imagePiece90 = imagePieceSized.RotateColorArrayCounterClockwise();
        this.colorImgPixData.Add(imagePiece90);
        var imagePiece180 = imagePiece90.RotateColorArrayCounterClockwise();
        this.colorImgPixData.Add(imagePiece180);
        var imagePiece270 = imagePiece180.RotateColorArrayCounterClockwise();
        this.colorImgPixData.Add(imagePiece270);

        this.processTile(imagePiece, this);

        lvResultTexture.umDestroyTexture();

        //int nPiecesHoriz = cols;
        //int nPiecesVert = rows;

        //int x = this.id % nPiecesHoriz;
        //int y = this.id / nPiecesHoriz;

        //double distancex = 0.0;
        //if (x < (nPiecesHoriz / 2))
        //{
        //    distancex = (double)(x) / (double)(nPiecesHoriz / 2);  // 0 - 1 to center
        //}
        //else
        //{
        //    distancex = 1 - (double)(x - nPiecesHoriz / 2) / (double)(nPiecesHoriz / 2);  // 1 - 0 from center
        //}

        //double distancey = 0.0;
        //if (y < (nPiecesVert / 2))
        //{
        //    distancey = (double)(y) / (double)(nPiecesVert / 2);  // 0 - 1 to center
        //}
        //else
        //{
        //    distancey = 1 - (double)(y - nPiecesVert / 2) / (double)(nPiecesVert / 2);  // 1 - 0 from center
        //}

        //print("id = %i, x y (%i %i), distance = %f \n", id, x, y, distanceFromCenter);
    }

    public static Texture2D ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
    {
        Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, false);

        //float incX = (1.0f / (float)targetWidth);
        //float incY = (1.0f / (float)targetHeight);

        for (int i = 0; i < result.height; ++i)
        {
            for (int j = 0; j < result.width; ++j)
            {
                Color newColor = source.GetPixelBilinear((float)j / (float)result.width, (float)i / (float)result.height);
                result.SetPixel(j, i, newColor);
            }
        }

        result.Apply();
        return result;
    }

    public static Texture2D cropTexture(Texture2D sourceTex, Rect sourceRect)
    {
        int x = Mathf.FloorToInt(sourceRect.x);
        int y = Mathf.FloorToInt(sourceRect.y);
        int width = Mathf.FloorToInt(sourceRect.width);
        int height = Mathf.FloorToInt(sourceRect.height);

        Color[] pix = sourceTex.GetPixels(x, y, width, height);
        Texture2D destTex = new Texture2D(width, height);
        destTex.SetPixels(pix);
        destTex.Apply();

        return destTex;
    }

    public static Texture2D rotateTexture(Texture2D tex, float angle)
    {
        Texture2D rotImage = new Texture2D(tex.width, tex.height);
        int x, y;
        float x1, y1, x2, y2;

        int w = tex.width;
        int h = tex.height;
        float x0 = rot_x(angle, -w / 2.0f, -h / 2.0f) + w / 2.0f;
        float y0 = rot_y(angle, -w / 2.0f, -h / 2.0f) + h / 2.0f;

        float dx_x = rot_x(angle, 1.0f, 0.0f);
        float dx_y = rot_y(angle, 1.0f, 0.0f);
        float dy_x = rot_x(angle, 0.0f, 1.0f);
        float dy_y = rot_y(angle, 0.0f, 1.0f);


        x1 = x0;
        y1 = y0;

        for (x = 0; x < tex.width; x++)
        {
            x2 = x1;
            y2 = y1;
            for (y = 0; y < tex.height; y++)
            {
                //rotImage.SetPixel (x1, y1, Color.clear);           

                x2 += dx_x;//rot_x(angle, x1, y1); 
                y2 += dx_y;//rot_y(angle, x1, y1); 
                rotImage.SetPixel((int)Mathf.Floor(x), (int)Mathf.Floor(y), getPixel(tex, x2, y2));
            }

            x1 += dy_x;
            y1 += dy_y;

        }

        rotImage.Apply();
        return rotImage;
    }

    private static Color getPixel(Texture2D tex, float x, float y)
    {
        Color pix;
        int x1 = (int)Mathf.Floor(x);
        int y1 = (int)Mathf.Floor(y);

        if (x1 > tex.width || x1 < 0 ||
         y1 > tex.height || y1 < 0)
        {
            pix = Color.clear;
        }
        else
        {
            pix = tex.GetPixel(x1, y1);
        }

        return pix;
    }

    private static float rot_x(float angle, float x, float y)
    {
        float cos = Mathf.Cos(angle / 180.0f * Mathf.PI);
        float sin = Mathf.Sin(angle / 180.0f * Mathf.PI);
        return (x * cos + y * (-sin));
    }

    private static float rot_y(float angle, float x, float y)
    {
        float cos = Mathf.Cos(angle / 180.0f * Mathf.PI);
        float sin = Mathf.Sin(angle / 180.0f * Mathf.PI);
        return (x * sin + y * cos);
    }
}

