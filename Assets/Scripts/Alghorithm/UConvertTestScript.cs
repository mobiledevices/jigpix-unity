﻿using System.Collections;
using U3D.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class UConvertTestScript : MonoBehaviour
{
    public RawImage ConvertImage;

    public Button NextButton;

    private Texture2D m_Source;
    private Texture2D m_Target;

    public const int COLS = 20;
    public const int ROWS = 26;

    private bool m_StopWorking = false;

    private Texture2D upConvertImageTexture
    {
        get
        {
            return ConvertImage.texture as Texture2D;
        }
        set
        {
            if (ConvertImage.texture != null)
            {
                Texture2D lvTexture2D = ConvertImage.texture as Texture2D;
                if (lvTexture2D != m_Source && lvTexture2D != m_Target)
                {
                    lvTexture2D.umDestroyTexture();
                }
            }

            ConvertImage.texture = value;
        }
    }

    public void Start()
    {
        if (NextButton)
        {
            NextButton.onClick.AddListener(umOnNextButtonClick);
        }
    }

    private void umOnNextButtonClick()
    {
        StartCoroutine(AlgorithmCoroutine());
    }

    private void umOnNextButtonClick2()
    {
        if (NextButton)
        {
            NextButton.enabled = false;
            m_StopWorking = false;

            m_Source = Resources.Load(UMainScript.upInstance.upMainData.upPuzzleArray[0]) as Texture2D;
            m_Target = Resources.Load(UMainScript.upInstance.upMainData.upPuzzleArray[1]) as Texture2D;

            upConvertImageTexture = m_Target;

            NextButton.GetComponentInChildren<Text>().text = "Preparing...";

            Puzzle lvPuzzle = new Puzzle();
            lvPuzzle.setup(COLS, rows: ROWS);

            lvPuzzle.setFromImage(m_Source, false);
            lvPuzzle.setFromImage(m_Target, true);

            lvPuzzle.centralImortance = 60.0;
            lvPuzzle.powerFunc = 3.0;

            NextButton.GetComponentInChildren<Text>().text = "Converting...";

            upConvertImageTexture = m_Source;

            Task<bool>.Run(() =>
            {
                //bool swapped = false;
                int refreshCnt = 10000;
                var cnt = 0;
                for (int i = 0; i < 1300000; i++)
                {
                    if (this.m_StopWorking)
                    {
                        break;
                    }

                    if (lvPuzzle.tryToSwapStuff())
                    {
                        //swapped = true;
                    }

                    cnt = cnt + 1;
                    if (cnt >= refreshCnt)
                    {
                        //self.changeImage();
                        Texture2D lvSomeTexture = saveOutputImage(lvPuzzle);
                        Task.RunInMainThread(() => { upConvertImageTexture = lvSomeTexture; });
                        cnt = 0;
                        //if (swapped == false)
                        //{
                        //    break;
                        //}
                        //swapped = false;
                    }
                }

                Texture2D lvResultTexture = this.saveOutputImage(lvPuzzle, m_Source.width, m_Source.height, true);
                Task.RunInMainThread(() => { upConvertImageTexture = lvResultTexture; });
            }).ContinueInMainThreadWith((t) =>
            {
                NextButton.GetComponentInChildren<Text>().text = "Convert";
                NextButton.enabled = true;
            });
        }
    }

    private IEnumerator AlgorithmCoroutine()
    {
        m_Source = Resources.Load(UMainScript.upInstance.upMainData.upPuzzleArray[0]) as Texture2D;
        m_Target = Resources.Load(UMainScript.upInstance.upMainData.upPuzzleArray[1]) as Texture2D;

        upConvertImageTexture = m_Target;

        NextButton.GetComponentInChildren<Text>().text = "Preparing...";

        yield return null;

        upConvertImageTexture = m_Source;

        Puzzle lvPuzzle = new Puzzle();
        lvPuzzle.setup(COLS, rows: ROWS);

        lvPuzzle.setFromImage(m_Source, false);
        lvPuzzle.setFromImage(m_Target, true);

        lvPuzzle.centralImortance = 60.0;
        lvPuzzle.powerFunc = 3.0;

        NextButton.GetComponentInChildren<Text>().text = "Converting...";

        yield return null;

        bool swapped = false;
        int refreshCnt = 10000;
        var cnt = 0;
        for (int i = 0; i < 1300000; i++)
        {
            if (this.m_StopWorking)
            {
                break;
            }

            if (lvPuzzle.tryToSwapStuff())
            {
                swapped = true;
            }

            cnt = cnt + 1;
            if (cnt >= refreshCnt)
            {
                Texture2D lvResultTexture2 = saveOutputImage(lvPuzzle);
                upConvertImageTexture = lvResultTexture2;
                cnt = 0;
                if (swapped == false)
                {
                    break;
                }
                swapped = false;

                yield return null;
            }
        }

        Texture2D lvResultTexture = this.saveOutputImage(lvPuzzle, m_Source.width, m_Source.height, true);
        upConvertImageTexture = lvResultTexture;

        NextButton.GetComponentInChildren<Text>().text = "Convert";
        NextButton.enabled = true;
    }

    public Texture2D saveOutputImage(Puzzle puzzle, int width = 0, int height = 0, bool last = false)
    {
        int pieceSizeW = puzzle.smallResHoriz / puzzle.nPiecesHoriz;
        int centers = (pieceSizeW); //* (last ? 2 : 1);

        Texture2D lvResultTexture = new Texture2D(centers * COLS, centers * ROWS);

        //for (int x = 0; x < lvResultTexture.width; x++)
        //{
        //    for (int y = 0; y < lvResultTexture.height; y++)
        //    {
        //        lvResultTexture.SetPixel(x, y, Color.black);
        //    }
        //}

        for (int p = 0; p < puzzle.nTotalPieces; p++)
        {
            int row = p / COLS;
            int col = p % COLS;

            var pieceImage = puzzle.pieces[p].img;//puzzle.pieces[p].colorImgPixData[puzzle.pieces[p].rotation];
            int lvRotation = puzzle.pieces[p].rotation;
            while (lvRotation > 0)
            {
                pieceImage = pieceImage.RotateColorArrayCounterClockwise();
                lvRotation--;
            }
            Rect drawRect = new Rect(col * centers, row * centers, centers, centers);
            lvResultTexture.SetPixels((int)drawRect.x, (int)drawRect.y, (int)drawRect.width, (int)drawRect.height, pieceImage.ConvertTwoToOneDimensionArray());
        }

        lvResultTexture.Apply();

        if (last)
        {
            var lvTmpResultTexture = lvResultTexture;
            lvResultTexture = PuzzlePiece.ScaleTexture(lvTmpResultTexture, width, height);
            lvTmpResultTexture.umDestroyTexture();

            //for (int x = 0; x < lvResultTexture.width; x++)
            //{
            //    for (int y = 0; y < lvResultTexture.height; y++)
            //    {
            //        if(x==0 || y==0 ||)
            //        lvResultTexture.SetPixel(x, y, Color.black);
            //    }
            //}
        }

        return lvResultTexture;
    }
}

