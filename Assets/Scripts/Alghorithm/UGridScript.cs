﻿using UnityEngine;
using UnityEngine.UI;

public class UGridScript : MonoBehaviour
{
    public RectTransform m_MaskRectTransform;
    public RawImage ConvertImage;

    private RectTransform m_RectTransform;
    private Canvas m_Canvas;

    private const string cStrArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private const int cLineThickness = 2;

    private float _zoomScale = 1.0f;

    private void Start()
    {
        m_RectTransform = GetComponent<RectTransform>();
        m_Canvas = GetComponentInParent<Canvas>();
    }

    private void OnGUI()
    {
        if (ConvertImage && ConvertImage.texture)
        {
            _zoomScale = ConvertImage.rectTransform.localScale.x;

            Rect lvImageMaskRect = m_MaskRectTransform.GetScreenRect(m_Canvas);

            GUI.contentColor = Color.black;
            GUIStyle lvTextStyle = new GUIStyle();
            lvTextStyle.fontSize = 20;
            lvTextStyle.alignment = TextAnchor.MiddleCenter;

            float wOff = Mathf.Min(40.0f, lvImageMaskRect.x); //m_RectTransform.offsetMin.x;
            float hOff = Mathf.Min(40.0f, lvImageMaskRect.x); //m_RectTransform.offsetMin.y;

            float w = (lvImageMaskRect.width) / UConvertTestScript.COLS;
            float h = (lvImageMaskRect.height) / UConvertTestScript.ROWS;

            float width = w * _zoomScale;
            float xOff = lvImageMaskRect.width * (_zoomScale - 1.0f) / 2.0f - ConvertImage.rectTransform.offsetMin.x * m_Canvas.scaleFactor;

            //Draw Columns
            for (int col = 0; col <= UConvertTestScript.COLS; col++)
            {
                //Top
                Rect rectangleT = new Rect(lvImageMaskRect.x + width * col - xOff, lvImageMaskRect.y - hOff, width, hOff);

                //Bottom
                Rect rectangleB = new Rect(lvImageMaskRect.x + width * col - xOff, lvImageMaskRect.y + lvImageMaskRect.height + hOff, width, -hOff);

                if (col != UConvertTestScript.COLS)
                {
                    string text = cStrArray.Substring(col, 1);
                    if (rectangleT.x >= lvImageMaskRect.x - cLineThickness && rectangleB.x <= lvImageMaskRect.xMax - width + cLineThickness)
                    {
                        GUI.Label(rectangleT, text, lvTextStyle);
                        GUI.Label(rectangleB, text, lvTextStyle);
                    }
                }

                if (rectangleT.x >= lvImageMaskRect.x - cLineThickness && rectangleB.x <= lvImageMaskRect.xMax + cLineThickness)
                    GuiHelper.DrawLine(new Vector2(rectangleT.x, rectangleT.y), new Vector2(rectangleB.x, rectangleB.y), GUI.contentColor, cLineThickness);
            }

            float height = h * _zoomScale;
            float yOff = lvImageMaskRect.height * (_zoomScale - 1.0f) / 2.0f + ConvertImage.rectTransform.offsetMax.y * m_Canvas.scaleFactor;

            //Draw Rows
            for (int row = 0; row <= UConvertTestScript.ROWS; row++)
            {
                //Left
                Rect rectangleL = new Rect(lvImageMaskRect.x - wOff, height * row + lvImageMaskRect.y - yOff, wOff, height);

                //Right
                Rect rectangleR = new Rect(lvImageMaskRect.x + lvImageMaskRect.width + wOff, height * row + lvImageMaskRect.y - yOff, -wOff, height);

                if (row != UConvertTestScript.ROWS)
                {
                    string text = (row + 1).ToString();
                    if (rectangleL.y >= lvImageMaskRect.y - cLineThickness && rectangleR.y <= lvImageMaskRect.yMax - height + cLineThickness)
                    {
                        GUI.Label(rectangleL, text, lvTextStyle);
                        GUI.Label(rectangleR, text, lvTextStyle);
                    }
                }

                if (rectangleL.y >= lvImageMaskRect.y - cLineThickness && rectangleR.y <= lvImageMaskRect.yMax + cLineThickness)
                    GuiHelper.DrawLine(new Vector2(rectangleL.x, rectangleL.y), new Vector2(rectangleR.x, rectangleR.y), GUI.contentColor, cLineThickness);
            }
        }
    }
}
