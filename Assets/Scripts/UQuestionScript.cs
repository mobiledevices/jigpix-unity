﻿using System;
using UnityEngine.UI;

public class UQuestionScript : UBasePanelScript
{
    public Button BackButton;
    public Button TakePhotoButton;
    public Button SelectPhotoButton;

    public override void Start()
    {
        base.Start();

        if (BackButton)
        {
            BackButton.onClick.AddListener(umOnBackButtonClick);
        }
        if (TakePhotoButton)
        {
            TakePhotoButton.onClick.AddListener(umOnTakePhotoButtonClick);
        }
        if (SelectPhotoButton)
        {
            SelectPhotoButton.onClick.AddListener(umOnSelectPhotoButtonClick);
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (BackButton)
        {
            BackButton.onClick.RemoveListener(umOnBackButtonClick);
        }
        if (TakePhotoButton)
        {
            TakePhotoButton.onClick.RemoveListener(umOnTakePhotoButtonClick);
        }
        if (SelectPhotoButton)
        {
            SelectPhotoButton.onClick.RemoveListener(umOnSelectPhotoButtonClick);
        }
    }

    private void umOnTakePhotoButtonClick()
    {
        UMainScript.upInstance.umSetPanel(UPanelType.TakePhoto, this);
    }

    private void umOnSelectPhotoButtonClick()
    {
        UMainScript.upInstance.umSetPanel(UPanelType.SelectPhoto, this);
    }
}
