﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

class UConvertScript : UBasePanelScript
{
    public Image ConvertImage;

    private Texture2D m_Source;
    private Texture2D m_Target;

    private readonly int COLS = 20;
    private readonly int ROWS = 26;

    private Puzzle m_Puzzle;

    private Sprite[] m_CodeSprites;

    private bool m_StopWorking = false;

    public override UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.Result;
        }
    }

    private Texture2D upConvertImageTexture
    {
        get
        {
            return ConvertImage.sprite.texture;
        }
        set
        {
            if (ConvertImage.sprite != null && ConvertImage.sprite.texture != null)
            {
                if (ConvertImage.sprite.texture != m_Source && ConvertImage.sprite.texture != m_Target)
                {
                    ConvertImage.sprite.texture.umDestroyTexture();
                }
            }

            ConvertImage.sprite = Sprite.Create(value, new Rect(0, 0, value.width, value.height), new Vector2(0.5f, 0.0f), 1.0f);
        }
    }

    public override void Start()
    {
        base.Start();

        m_CodeSprites = Resources.LoadAll<Sprite>("codes_new");
    }

    public override void OnEnable()
    {
        base.OnEnable();

        UMainData lvMainData = UMainScript.upInstance.upMainData;
        string[] lvPuzzleArray = lvMainData.upPuzzleArray;
        string lvPuzzleItem = lvPuzzleArray[lvMainData.upSelectedPuzzleIndex];
        Object lvImageObject = Resources.Load(lvPuzzleItem);
        Texture2D lvTexture2D = lvImageObject as Texture2D;

        m_Source = lvTexture2D;

        m_Target = UMainScript.upInstance.upMainData.upSelectedPhotoRawData.umCreateTexture();

        if (ConvertImage)
        {
            upConvertImageTexture = m_Target;
        }
        if (NextButton)
        {
            NextButton.GetComponentInChildren<Text>().text = "Convert";
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();

        m_Target.umDestroyTexture();
    }

    protected override void umOnNextButtonClick()
    {
        if (NextButton)
        {
            NextButton.enabled = false;
            m_StopWorking = false;
            StartCoroutine(AlgorithmCoroutine());
        }
    }

    public override void OnDestroy()
    {
        UMainScript.upInstance.upMainData.upSelectedPhotoRawData = null;
        UMainScript.upInstance.upMainData.upCodeRawData = null;
        UMainScript.upInstance.upMainData.upConvertedRawData = null;

        m_Puzzle = null;

        base.OnDestroy();
    }

    protected override void umOnBackButtonClick()
    {
        m_StopWorking = true;
        base.umOnBackButtonClick();
    }

    private IEnumerator AlgorithmCoroutine()
    {
        upConvertImageTexture = m_Source;

        NextButton.GetComponentInChildren<Text>().text = "Preparing...";

        yield return null;

        Resources.UnloadUnusedAssets();

        m_Puzzle = new Puzzle();
        m_Puzzle.setup(COLS, rows: ROWS);

        m_Puzzle.setFromImage(m_Source, false);
        m_Puzzle.setFromImage(m_Target, true);

        m_Puzzle.centralImortance = 60.0;
        m_Puzzle.powerFunc = 3.0;

        NextButton.GetComponentInChildren<Text>().text = "Converting...";

        yield return null;

        bool swapped = false;
        int refreshCnt = 10000;
        var cnt = 0;
        for (int i = 0; i < 1300000; i++)
        {
            if (this.m_StopWorking)
            {
                break;
            }

            if (m_Puzzle.tryToSwapStuff())
            {
                swapped = true;
            }

            cnt = cnt + 1;
            if (cnt >= refreshCnt)
            {
                //self.changeImage();
                saveOutputImage();
                cnt = 0;
                if (swapped == false)
                {
                    break;
                }
                swapped = false;

                yield return null;
            }
        }

        this.saveOutputImage(m_Source.width, m_Source.height, true);

        UMainScript.upInstance.upMainData.upConvertedRawData = upConvertImageTexture.EncodeToPNG();
        UMainScript.upInstance.upMainData.upCodeRawData = generateCodeImage();

        NextButton.GetComponentInChildren<Text>().text = "Convert";
        NextButton.enabled = true;

        base.umOnNextButtonClick();

        Resources.UnloadUnusedAssets();
    }

    private void saveOutputImage(int width = 0, int height = 0, bool last = false)
    {
        int pieceSizeW = m_Puzzle.smallResHoriz / m_Puzzle.nPiecesHoriz;
        int centers = (pieceSizeW); //* (last ? 2 : 1);

        Texture2D lvResultTexture = new Texture2D(centers * COLS, centers * ROWS);

        for (int x = 0; x < lvResultTexture.width; x++)
        {
            for (int y = 0; y < lvResultTexture.height; y++)
            {
                lvResultTexture.SetPixel(x, y, Color.black);
            }
        }

        for (int p = 0; p < m_Puzzle.nTotalPieces; p++)
        {
            int row = p / COLS;
            int col = p % COLS;

            var pieceImage = m_Puzzle.pieces[p].img;//puzzle.pieces[p].colorImgPixData[puzzle.pieces[p].rotation];
            int lvRotation = m_Puzzle.pieces[p].rotation;
            while (lvRotation > 0)
            {
                pieceImage = pieceImage.RotateColorArrayCounterClockwise();
                lvRotation--;
            }
            Rect drawRect = new Rect(col * centers, row * centers, centers, centers);
            lvResultTexture.SetPixels((int)drawRect.x, (int)drawRect.y, (int)drawRect.width, (int)drawRect.height, pieceImage.ConvertTwoToOneDimensionArray());
        }

        lvResultTexture.Apply();

        if (last)
        {
            var lvTmpResultTexture = lvResultTexture;
            lvResultTexture = PuzzlePiece.ScaleTexture(lvTmpResultTexture, width, height);
            lvTmpResultTexture.umDestroyTexture();
        }

        upConvertImageTexture = lvResultTexture;
    }

    private byte[] generateCodeImage()
    {
        int centers = (int)m_CodeSprites[0].textureRect.width;
        Texture2D lvResultTexture = new Texture2D(centers * COLS, centers * ROWS);

        for (int x = 0; x < lvResultTexture.width; x++)
        {
            for (int y = 0; y < lvResultTexture.height; y++)
            {
                lvResultTexture.SetPixel(x, y, Color.black);
            }
        }

        for (int p = 0; p < m_Puzzle.nTotalPieces; p++)
        {
            int row = p / m_Puzzle.nPiecesHoriz;
            int col = p % m_Puzzle.nPiecesHoriz;
            PuzzlePiece piece = m_Puzzle.pieces[p];
            int pieceNo = piece.id;

            Texture2D pieceImage = PuzzlePiece.cropTexture(m_CodeSprites[pieceNo].texture, m_CodeSprites[pieceNo].textureRect);
            int degrees = m_Puzzle.pieces[p].rotation * 90;
            if (degrees > 0)
            {
                Texture2D lvTmpTexture = pieceImage;
                pieceImage = PuzzlePiece.rotateTexture(lvTmpTexture, degrees);
                lvTmpTexture.umDestroyTexture();
            }
            Rect drawRect = new Rect(col * centers, row * centers, centers, centers);
            lvResultTexture.SetPixels((int)drawRect.x, (int)drawRect.y, (int)drawRect.width, (int)drawRect.height, pieceImage.GetPixels());
            pieceImage.umDestroyTexture();
        }

        lvResultTexture.Apply();
        byte[] lvRawData = lvResultTexture.EncodeToPNG();
        lvResultTexture.umDestroyTexture();

        return lvRawData;
    }
}
