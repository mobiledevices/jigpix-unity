﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tastybits.CamCap;
using UnityEngine;
using UnityEngine.UI;

public class UTakePhotoScript : UBasePanelScript
{
    public CameraCaptureView PhotoImage;
    public Button ChangeCameraButton;
    public Button FlashlightButton;

    private WebCamTexture m_WebCamTexture;
    private int m_DeviceIdx = 0;

    private bool m_FlashLightOn = false;

    public override UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.Convert;
        }
    }

    public override void Start()
    {
        base.Start();

        if (ChangeCameraButton)
        {
            ChangeCameraButton.enabled = WebCamTexture.devices.Length > 1;
            ChangeCameraButton.onClick.AddListener(umOnChangeCameraButtonClick);
        }
        if (FlashlightButton)
        {
            FlashlightButton.onClick.AddListener(umOnFlashlightButtonClick);
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (ChangeCameraButton)
        {
            ChangeCameraButton.onClick.RemoveListener(umOnChangeCameraButtonClick);
        }
        if (FlashlightButton)
        {
            FlashlightButton.onClick.RemoveListener(umOnFlashlightButtonClick);
        }
    }

    public override void OnEnable()
    {
        base.OnEnable();

        if (m_WebCamTexture == null)
        {
            m_WebCamTexture = new WebCamTexture((int)PhotoImage.rectTransform.rect.width, (int)PhotoImage.rectTransform.rect.height);
        }

        if (WebCamTexture.devices.Length > 0)
        {
            StartCoroutine(umPlayCamera());
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();

        if (m_WebCamTexture && m_WebCamTexture.isPlaying)
        {
            m_WebCamTexture.Stop();
        }
    }

    protected override void umOnNextButtonClick()
    {
        if (PhotoImage && PhotoImage.texture)
        {
            ////RectTransform lvParentRectTransform = PhotoImage.GetComponentInParent<Mask>().GetComponent<RectTransform>();
            //////ShowToast.showToastOnUiThread(string.Format("Width = {0} Height = {1}", m_WebCamTexture.width, m_WebCamTexture.height));
            //int lvWidth = m_WebCamTexture.width;//(int)(m_WebCamTexture.height * (lvParentRectTransform.rect.width / lvParentRectTransform.rect.height));
            //int lvHeight = m_WebCamTexture.height;
            //int lvX = 0;//(lvHeight - lvWidth);
            //int lvY = 0;
            //Texture2D lvSelectedPhotoTexture = new Texture2D(lvWidth, lvHeight);
            //lvSelectedPhotoTexture.SetPixels(m_WebCamTexture.GetPixels(lvX, lvY, lvWidth, lvHeight));
            //lvSelectedPhotoTexture.Apply();
            //if (m_WebCamTexture.videoRotationAngle == 90)
            //    lvSelectedPhotoTexture = lvSelectedPhotoTexture.RotatedLeft();
            //else if (m_WebCamTexture.videoRotationAngle == 270)
            //    lvSelectedPhotoTexture = lvSelectedPhotoTexture.RotatedRight();
            //else if (m_WebCamTexture.videoRotationAngle == 180)
            //    lvSelectedPhotoTexture = lvSelectedPhotoTexture.RotatedLeft();
            CopyRotate lvCopyRotate = CopyRotate.None;
            if (m_WebCamTexture.videoRotationAngle == 90)
                lvCopyRotate = CopyRotate.Left;
            else if (m_WebCamTexture.videoRotationAngle == 180)
                lvCopyRotate = CopyRotate.LeftX2;
            else if (m_WebCamTexture.videoRotationAngle == 270)
                lvCopyRotate = CopyRotate.Right;
            CopyFlip lvCopyFlip = CopyFlip.None;
            if (IsCurrentFrontFacing() || (Application.platform == RuntimePlatform.Android && m_WebCamTexture.videoVerticallyMirrored))
                lvCopyFlip = CopyFlip.Horisontal;

            Texture2D lvSelectedPhotoTexture = m_WebCamTexture.Copy(lvCopyRotate, lvCopyFlip);

            if (UMainScript.upInstance.upMainData.upSettingsData.upSaveOriginalPhotos)
			{
				if (Application.platform == RuntimePlatform.IPhonePlayer)
				{
                    IOSCamera.Instance.SaveTextureToCameraRoll(lvSelectedPhotoTexture);
                }
				else if (Application.platform == RuntimePlatform.Android)
				{
                    AndroidCamera.Instance.SaveImageToGallery(lvSelectedPhotoTexture);
                }
			}

            UMainScript.upInstance.upMainData.upSelectedPhotoRawData = lvSelectedPhotoTexture.EncodeToPNG();
            lvSelectedPhotoTexture.umDestroyTexture();
            base.umOnNextButtonClick();
        }
    }

    private void umOnFlashlightButtonClick()
    {
        try
        {
            //m_WebCamTexture.SetFlashMode(m_FlashLightOn ? FlashModes.Off : FlashModes.On);
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                //CameraCapture.fl
            }
            else if (Application.platform == RuntimePlatform.Android)
            {
                //AndroidJavaClass cameraClass = new AndroidJavaClass("android.hardware.Camera");
                //AndroidJavaObject camera = cameraClass.CallStatic<AndroidJavaObject>("open", 0);
                //AndroidJavaObject cameraParameters = camera.Call<AndroidJavaObject>("getParameters");
                //cameraParameters.Call("setFlashMode", "torch");
                //camera.Call("setParameters", cameraParameters);
            }
            m_FlashLightOn = !m_FlashLightOn;
        }
        catch (Exception lvExc)
        {
            ShowToast.showToastOnUiThread(lvExc.Message);
        }
    }

    private void umOnChangeCameraButtonClick()
    {
        if (m_WebCamTexture || m_WebCamTexture.isPlaying)
        {
            m_WebCamTexture.Stop();

            m_DeviceIdx += 1;
            if (WebCamTexture.devices.Length <= m_DeviceIdx)
                m_DeviceIdx = 0;

            StartCoroutine(umPlayCamera());
        }
    }

    private bool IsCurrentFrontFacing()
    {
        return WebCamTexture.devices[m_DeviceIdx].isFrontFacing;
    }

    private bool IsCurrentBackFacing()
    {
        return !WebCamTexture.devices[m_DeviceIdx].isFrontFacing;
    }

    private IEnumerator umPlayCamera()
    {
        m_WebCamTexture.deviceName = WebCamTexture.devices[m_DeviceIdx].name;
        m_WebCamTexture.Play();
        PhotoImage.texture = null;

        yield return new WaitForSeconds(1.0f);

        PhotoImage.SetFlipY(false);
        PhotoImage.SetFlipAfterRotate(false);
        //PhotoImage.SetNumberOfTimesRotated90Degrees(-PhotoImage.nRotate90);

        #region Aspect Ratio

        AspectRatioFitter asf = PhotoImage.GetComponent<AspectRatioFitter>();
        int w = m_WebCamTexture.width;
        int h = m_WebCamTexture.height;
        if (m_WebCamTexture.videoRotationAngle == 90 || m_WebCamTexture.videoRotationAngle == 270)
        {
            h = m_WebCamTexture.width;
            w = m_WebCamTexture.height;
        }
        float ratio = (float)w / (float)h;
        asf.enabled = true;
        asf.aspectRatio = ratio;
        asf.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;

        #endregion


        int rotAngle = Application.platform == RuntimePlatform.Android ? -m_WebCamTexture.videoRotationAngle : m_WebCamTexture.videoRotationAngle;
        int numRotate = rotAngle / 90;
        PhotoImage.SetNumberOfTimesRotated90Degrees(numRotate);
        PhotoImage.SetFlipY((IsCurrentFrontFacing() && Application.platform != RuntimePlatform.Android) || (m_WebCamTexture.videoVerticallyMirrored));
        PhotoImage.SetFlipAfterRotate(IsCurrentFrontFacing());

        PhotoImage.texture = m_WebCamTexture;
        //ShowToast.showToastOnUiThread(string.Format("Rotation angle = {0} Number = {1}", m_WebCamTexture.videoRotationAngle, PhotoImage.nRotate90));
    }
}
