﻿using UnityEngine;

public enum UPanelType
{
    Unknown,
    PickYourPuzzle,
    //EnterCode,
    YourPuzzle,
    Question,
    TakePhoto,
    SelectPhoto,
    Convert,
    Result,
    Code,
    Settings,
    Info
}

public class UMainScript : MonoBehaviour
{
    public UMainPanelScript MainPanel;

    private static UMainScript m_Instance = null;         //Static instance of UMainScript.

    private UMainData m_MainData;

    #region Property

    // Static instance of UMainScript which allows it to be accessed by any other script.
    public static UMainScript upInstance
    {
        get
        {
            return m_Instance;
        }
    }

    public UMainData upMainData
    {
        get
        {
            if (m_MainData == null)
                m_MainData = new UMainData();

            return m_MainData;
        }
    }

    #endregion

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (m_Instance == null)
            //if not, set instance to this
            m_Instance = this;

        //If instance already exists and it's not this:
        else if (m_Instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a VApplicationManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        Application.targetFrameRate = 60;

#if UNITY_IPHONE
  UnityEngine.iOS.Device.SetNoBackupFlag(Application.persistentDataPath);
#endif
    }

    public GameObject umSetPanel(UPanelType panelType, UBasePanelScript prevPanel)
    {
        GameObject lvView = null;

        if (MainPanel)
            lvView = MainPanel.umSetPanel(panelType, prevPanel);

        return lvView;
    }
}