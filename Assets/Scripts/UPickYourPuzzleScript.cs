﻿using UnityEngine;
using UnityEngine.UI;

public class UPickYourPuzzleScript : UBasePanelScript
{
    public GridLayoutGroup GridLayoutGroup;
    public Button ImageButtonTemplate;

    private Button[] m_ImageButtonArray;

    public override UPanelType upNextPanelType
    {
        get
        {
            return UPanelType.YourPuzzle;//EnterCode;
        }
    }

    public override bool upAnimate
    {
        get
        {
            return false;
        }
    }

    public override void Start()
    {
        base.Start();

        if (m_BackButton)
            m_BackButton.gameObject.SetActive(false);

        string[] lvPuzzleArray = UMainScript.upInstance.upMainData.upPuzzleArray;
        m_ImageButtonArray = new Button[4];//lvPuzzleArray.Length];
        for (int i = 0; i < m_ImageButtonArray.Length; i++)
        {
            if (GridLayoutGroup)
            {
                if (ImageButtonTemplate)
                {
                    Button lvImageButton = Button.Instantiate(ImageButtonTemplate, this.GridLayoutGroup.transform) as Button;
                    lvImageButton.name = "ImageButton" + (i + 1).ToString();
                    lvImageButton.gameObject.SetActive(true);

                    string lvPuzzleItem = lvPuzzleArray[i];
                    Object lvImageObject = Resources.Load(lvPuzzleItem);
                    Texture2D lvTexture2D = lvImageObject as Texture2D;
                    Sprite lvSprite = Sprite.Create(lvTexture2D, new Rect(0, 0, lvTexture2D.width, lvTexture2D.height), new Vector2(0.5f, 0.0f), 1.0f);

                    lvImageButton.image.sprite = lvSprite;
                    lvImageButton.onClick.AddListener(delegate { umOnImageButtonClick(lvImageButton); });
                    m_ImageButtonArray[i] = lvImageButton;
                }
            }
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (GridLayoutGroup)
        {
            Button[] lvImageButtons = GridLayoutGroup.GetComponentsInChildren<Button>();

            foreach (Button lvImageButton in lvImageButtons)
            {
                lvImageButton.onClick.RemoveListener(delegate { umOnImageButtonClick(null); });
            }
        }
    }

    private void umOnImageButtonClick(Button sender)
    {
        if (sender)
        {
            Button[] lvImageButtons = GridLayoutGroup.GetComponentsInChildren<Button>();

            string[] lvPuzzleArray = UMainScript.upInstance.upMainData.upPuzzleArray;
            for (int i = 0; i < lvImageButtons.Length; i++)
            {
                if (lvImageButtons[i] == sender)
                    UMainScript.upInstance.upMainData.upSelectedPuzzleIndex = i;
            }
            umOnNextButtonClick();
        }
    }
}
