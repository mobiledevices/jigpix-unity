﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class USettingsScript : UBasePanelScript
{
    public Button BackButton;
    public Toggle SaveOriginalPhotosToggle;
    public Toggle AutoSavePuzzleArtworkToggle;
    public Button RateTheAppButton;
    public Button TermsOfUseButton;
    public Button PrivacyPolicyButton;

    public override void Start()
    {
        base.Start();

        if (BackButton)
        {
            BackButton.onClick.AddListener(umOnBackButtonClick);
        }
        if (SaveOriginalPhotosToggle)
        {
            SaveOriginalPhotosToggle.onValueChanged.AddListener(umSaveOriginalPhotosToggleValueChanged);
            SaveOriginalPhotosToggle.isOn = UMainScript.upInstance.upMainData.upSettingsData.upSaveOriginalPhotos;
        }
        if (AutoSavePuzzleArtworkToggle)
        {
            AutoSavePuzzleArtworkToggle.onValueChanged.AddListener(umAutoSavePuzzleArtworkToggleValueChanged);
            AutoSavePuzzleArtworkToggle.isOn = UMainScript.upInstance.upMainData.upSettingsData.upAutoSavePuzzleArtwork;
        }
        if (RateTheAppButton)
        {
            RateTheAppButton.onClick.AddListener(umOnRateTheAppButtonClick);
        }
        if (TermsOfUseButton)
        {
            TermsOfUseButton.onClick.AddListener(umOnTermsOfUseButtonClick);
        }
        if (PrivacyPolicyButton)
        {
            PrivacyPolicyButton.onClick.AddListener(umOnPrivacyPolicyButtonClick);
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (BackButton)
        {
            BackButton.onClick.RemoveListener(umOnBackButtonClick);
        }
        if (SaveOriginalPhotosToggle)
        {
            SaveOriginalPhotosToggle.onValueChanged.RemoveListener(umSaveOriginalPhotosToggleValueChanged);
        }
        if (AutoSavePuzzleArtworkToggle)
        {
            AutoSavePuzzleArtworkToggle.onValueChanged.RemoveListener(umAutoSavePuzzleArtworkToggleValueChanged);
        }
        if (RateTheAppButton)
        {
            RateTheAppButton.onClick.RemoveListener(umOnRateTheAppButtonClick);
        }
        if (TermsOfUseButton)
        {
            TermsOfUseButton.onClick.RemoveListener(umOnTermsOfUseButtonClick);
        }
        if (PrivacyPolicyButton)
        {
            PrivacyPolicyButton.onClick.RemoveListener(umOnPrivacyPolicyButtonClick);
        }
    }

    public override void Update()
    {
        //base.Update();
    }

    private void umSaveOriginalPhotosToggleValueChanged(bool switched)
    {
        UMainScript.upInstance.upMainData.upSettingsData.upSaveOriginalPhotos = switched;
    }

    private void umAutoSavePuzzleArtworkToggleValueChanged(bool switched)
    {
        UMainScript.upInstance.upMainData.upSettingsData.upAutoSavePuzzleArtwork = switched;
    }

    private void umOnRateTheAppButtonClick()
    {
#if UNITY_ANDROID
 Application.OpenURL("market://details?id=" + Application.bundleIdentifier);
#elif UNITY_IPHONE
 Application.OpenURL("itms-apps://itunes.apple.com/app/id1068841832"); //YOUR_ID = "1068841832"
#endif
    }

    private void umOnTermsOfUseButtonClick()
    {
        umShowInfo("Terms of Use", string.Empty);
    }

    private void umOnPrivacyPolicyButtonClick()
    {
        umShowInfo("Privacy Policy", string.Empty);
    }

    private void umShowInfo(string header, string body)
    {
        GameObject lvInfoView = UMainScript.upInstance.umSetPanel(UPanelType.Info, this);
        if (lvInfoView)
        {
            InfoBehaviourScript lvInfoBehaviourScript = lvInfoView.GetComponent<InfoBehaviourScript>();
            if (lvInfoBehaviourScript)
            {
                lvInfoBehaviourScript.HeaderText.text = header;
                //lvInfoBehaviourScript.BodyText.text = body;
            }
        }
    }
}