﻿using UnityEngine;

public static class UTextureEx
{
    public static Texture2D umCreateTexture(this byte[] rawData)
    {
        // Create a texture. Texture size does not matter, since
        // LoadImage will replace with with incoming image size.
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(rawData);

        return tex;
    }

    public static void umDestroyTexture(this Texture2D texture)
    {
        try
        {
            Texture2D.DestroyImmediate(texture);
        }
        catch(System.Exception exc)
        {

        }
    }

    public static Color32[,] ScaleTextureColors(this Texture2D texture, int targetWidth, int targetHeight)
    {
        Color32[,] lvResult = new Color32[targetWidth, targetHeight];

        for (int i = 0; i < targetHeight; ++i)
        {
            for (int j = 0; j < targetWidth; ++j)
            {
                Color newColor = texture.GetPixelBilinear((float)j / (float)targetWidth, (float)i / (float)targetHeight);
                lvResult[j, i] = newColor;
            }
        }
        return lvResult;
    }

    public static Color32[,] CropColorArray(this Color32[,] sourceArray, Rect sourceRect)
    {
        Color32[,] lvResult = new Color32[(int)sourceRect.width, (int)sourceRect.height];
        for (int i = 0; i < sourceRect.width; i++)
        {
            for (int j = 0; j < sourceRect.height; j++)
            {
                lvResult[i, j] = sourceArray[(int)sourceRect.x + i, (int)sourceRect.y + j];
            }
        }

        return lvResult;
    }

    public static Color32[,] RotateColorArrayCounterClockwise(this Color32[,] oldMatrix)
    {
        Color32[,] newMatrix = new Color32[oldMatrix.GetLength(1), oldMatrix.GetLength(0)];
        int newColumn, newRow = 0;
        for (int oldColumn = oldMatrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
        {
            newColumn = 0;
            for (int oldRow = 0; oldRow < oldMatrix.GetLength(0); oldRow++)
            {
                newMatrix[newRow, newColumn] = oldMatrix[oldRow, oldColumn];
                newColumn++;
            }
            newRow++;
        }
        return newMatrix;
    }

    public static Color[] ConvertTwoToOneDimensionArray(this Color32[,] twoArray)
    {
        int width = twoArray.GetLength(0);
        int height = twoArray.GetLength(1);
        Color[] oneArray = new Color[width * height];
        Color32 color;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                color = twoArray[j, i];
                oneArray[i * height + j] = color;
            }
        }
        return oneArray;
    }
}
