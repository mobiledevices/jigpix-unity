﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowToast : MonoBehaviour
{
    private static string toastString;
    private static string input;

    public void ToAndroidClipBoard(string txt)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            input = txt;
            AndroidJavaObject unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(toClipBoardOnUiThread));
        }
    }

    private void toClipBoardOnUiThread()
    {
        AndroidJavaClass Context = new AndroidJavaClass("android.content.Context");
        AndroidJavaObject CLIPBOARD_SERVICE = Context.GetStatic<AndroidJavaObject>("CLIPBOARD_SERVICE");
        AndroidJavaObject unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject clipboardMgr = currentActivity.Call<AndroidJavaObject>("getSystemService", CLIPBOARD_SERVICE);
        AndroidJavaClass ClipData = new AndroidJavaClass("android.content.ClipData");
        AndroidJavaObject clipData = ClipData.CallStatic<AndroidJavaObject>("newPlainText", "deltaBit", input);
        clipboardMgr.Call("setPrimaryClip", clipData);

        showToastOnUiThread("Copied to clipboard: " + input);
    }

    public static void showToastOnUiThread(string toastStr)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            toastString = toastStr;
            AndroidJavaObject unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
        }
    }

    private static void showToast()
    {
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
        AndroidJavaObject unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_LONG"));
        toast.Call("show");
    }

}