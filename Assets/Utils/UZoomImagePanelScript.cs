﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class UZoomImagePanelScript : MonoBehaviour
{
    public ScrollRect ScrollRectPanel;
    private RawImage ZoomableImage;

    //private float currentScale = 1.0f;
    private float minScale = 1.0f;
    private float maxScale = 6.0f;

    private Vector3 originalPos;

    private float zoomSpeed = 0.05f;        // The rate of change of the canvas scale factor

    private void Start()
    {
        ZoomableImage = GetComponent<RawImage>();
        umSetOriginalPosition();
    }

    public void umSetOriginalPosition()
    {
        originalPos = ZoomableImage.transform.position;
    }

    private void Update()
    {
        umCheckPinch();
    }

    private void umZoom(float direction, bool pinching)
    {
        if (direction > 0)
        {
            if (ZoomableImage.transform.localScale.x < maxScale)
            {
                ZoomableImage.transform.localScale += Vector3.one * 0.3f * 1 * (pinching ? 0.3f : 1);
            }
            else
            {
                ZoomableImage.transform.localScale = Vector3.one * maxScale;
            }
        }
        else if (direction < 0)
        {
            if (ZoomableImage.transform.localScale.x > minScale)
            {
                ZoomableImage.transform.localScale += Vector3.one * 0.3f * -1 * (pinching ? 0.3f : 1);
            }
            else
            {
                ZoomableImage.transform.localScale = Vector3.one * minScale;
            }
        }
    }

    private void umCheckPinch()
    {
        if (Input.touchCount == 2)
        {
            if (ScrollRectPanel)
            {
                if (ScrollRectPanel.horizontal)
                    ScrollRectPanel.horizontal = false;
                if (ScrollRectPanel.vertical)
                    ScrollRectPanel.vertical = false;
            }

            Touch tOne = Input.GetTouch(0);
            Touch tTwo = Input.GetTouch(1);

            Vector2 tOnePrevPos = tOne.position - tOne.deltaPosition;
            Vector2 tTwoPrevPos = tTwo.position - tTwo.deltaPosition;

            float movementThreshold = 1f;

            if (tOne.deltaPosition.magnitude > movementThreshold && tTwo.deltaPosition.magnitude > movementThreshold)
            {
                float prevDist = (tOnePrevPos - tTwoPrevPos).magnitude;
                float currDist = (tOne.position - tTwo.position).magnitude;

                float change = currDist - prevDist;
                umZoom(change, true);
            }
        }
        else if (umIsMouseDoubleClick() || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(0).tapCount == 2))
        {
            ZoomableImage.transform.localScale = Vector3.one;
            ZoomableImage.transform.position = originalPos;
        }
        else
        {
            float increment = Input.GetAxis("Mouse ScrollWheel");

            if (increment != 0.0f)
            {
                if (ScrollRectPanel)
                {
                    if (ScrollRectPanel.horizontal)
                        ScrollRectPanel.horizontal = false;
                    if (ScrollRectPanel.vertical)
                        ScrollRectPanel.vertical = false;

                    umZoom(increment, true);
                }
            }
            else
            {
                if (ScrollRectPanel)
                {
                    if (!ScrollRectPanel.horizontal)
                        ScrollRectPanel.horizontal = true;
                    if (!ScrollRectPanel.vertical)
                        ScrollRectPanel.vertical = true;
                }
            }
        }
    }

    #region Double Click

    float clicked = 0;
    float clicktime = 0;
    float clickdelay = 0.5f;

    private bool umIsMouseDoubleClick()
    {
        bool result = false;

        if (Input.GetMouseButtonDown(0))
        {
            clicked++;
            if (clicked == 1) clicktime = Time.time;
        }
        if (clicked > 1 && Time.time - clicktime < clickdelay)
        {
            clicked = 0;
            clicktime = 0;
            result = true;
        }
        else if (clicked > 2 || Time.time - clicktime > 1)
            clicked = 0;

        return result;
    } 

    #endregion
}

