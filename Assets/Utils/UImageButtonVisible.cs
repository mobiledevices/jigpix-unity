﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UImageButtonVisible : MonoBehaviour
{
    private RawImage m_Image;
    private RectTransform m_RectTransform;
    private ScrollRect m_scrollRect;

    private string m_LoadPath;

    private bool m_LoadingImage = false;

    public string upLoadPath
    {
        set
        {
            m_LoadPath = value;
        }
    }

    private bool upCanLoad
    {
        get
        {
            return upTexture == null && !string.IsNullOrEmpty(m_LoadPath) && m_LoadingImage == false;
        }
    }

    public Texture2D upTexture
    {
        get
        {
            return m_Image.texture as Texture2D;
        }
        set
        {
            m_Image.texture = value;
        }
    }

    private void Start()
    {
        m_Image = GetComponent<RawImage>();
        m_RectTransform = GetComponent<RectTransform>();
        m_scrollRect = GetComponentInParent<ScrollRect>();
    }

    private void Update()
    {
        if (upCanLoad)
        {
            if (RectTransformUtility.RectangleContainsScreenPoint(m_scrollRect.transform as RectTransform, m_RectTransform.position))
            {
                // Do something that re-enable UI elements
                umDoLoadImage();
            }
            else
            {
                // Do Something that disable UI elements
                //upTexture = null;
            }
        }
    }

    private void OnDestroy()
    {
        if (upTexture)
            upTexture = null;
            //upTexture.umDestroyTexture();
    }

    private void umDoLoadImage()
    {
        m_LoadingImage = true;

        //if (Application.platform == RuntimePlatform.Android)
        //    StartCoroutine(umLoadImageAndroid());
        //else
        StartCoroutine(umLoadImage());
    }

    private IEnumerator umLoadImageAndroid()
    {
        try
        {
            WWW www = new WWW(m_LoadPath);

            yield return www;

            //Texture2D texTmp = new Texture2D(2, 2, TextureFormat.RGB24, false);
            //www.LoadImageIntoTexture(texTmp);
            upTexture = www.texture;
        }
        finally
        {
            m_LoadingImage = false;
        }
    }

    private IEnumerator umLoadImage()
    {
        try
        {
            Object lvImageObject = Resources.Load(m_LoadPath);

            yield return lvImageObject;

            Texture2D lvTexture2D = lvImageObject as Texture2D;
            upTexture = lvTexture2D;
        }
        finally
        {
            m_LoadingImage = false;
        }
    }
}
