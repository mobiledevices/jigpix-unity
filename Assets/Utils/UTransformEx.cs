﻿using UnityEngine;

public static class UTransformEx
{
    public static Transform umClear(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }
}
