===========================================================================
  NOTES - ANDROID
===========================================================================

In order to be able to save the images to the Android Album you must be sure to add a WRITE_EXTERNAL_STORAGE permissino to the Android Manifests.

If you have no other AndroidManifest.xml on the Asset path. Assets/Plugins/Android/AndroidManifest.xml you can just copy 
 the provided manifest residing on Tastybits/CameraCaptureKit/Native/Android/AndroidManifest.xml to the path Assets/Plugins/AndroidManifest.xml

Otherwise, you need to add the WRITE_EXTERNAL_STORAGE manually to the manifest by adding the following snippet to your existing manifest.

<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

The snippet must be added as a child element to the <application> element ( like the manifest residing in Tastybits/CameraCaptureKit/Native/Android/AndroidManifest.xml )




   




