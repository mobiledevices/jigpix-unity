﻿public class UMainData
{
    private string[] m_PuzzleArray = new string[] { "Venus-Birth", "08-Tiger", "Wallace-Gromit", "01-Trump", "02-Corbyn", "03-Markel", "04-Berlusconi", "05-Johnson", "06-Farage", "07-Putin", "Mona-Liza", "09-Chicken", "10-Fox", "11-Giraffe", "12-Orangutan", "13-Owl", "14-Zebra" };
    //private string[] m_PuzzleArray = new string[] { "pic-01", "pic-03", "pic-05", "pic-02", "pic-04", "pic-06" };

    private int m_SelectedPuzzleIndex = -1;

    private byte[] m_SelectedPhotoRawData;

    private byte[] m_ConvertedRawData;

    private byte[] m_CodeRawData;

    private USettingsData m_SettingsData;

    public string[] upPuzzleArray
    {
        get
        {
            return m_PuzzleArray;
        }
    }

    public int upSelectedPuzzleIndex
    {
        get
        {
            return m_SelectedPuzzleIndex;
        }
        set
        {
            m_SelectedPuzzleIndex = value;
        }
    }

    public USettingsData upSettingsData
    {
        get
        {
            if (m_SettingsData == null)
                m_SettingsData = new USettingsData();

            return m_SettingsData;
        }
    }

    public byte[] upSelectedPhotoRawData
    {
        get
        {
            return m_SelectedPhotoRawData;
        }
        set
        {
            m_SelectedPhotoRawData = value;
        }
    }

    public byte[] upConvertedRawData
    {
        get
        {
            return m_ConvertedRawData;
        }
        set
        {
            m_ConvertedRawData = value;
        }
    }

    public byte[] upCodeRawData
    {
        get
        {
            return m_CodeRawData;
        }
        set
        {
            m_CodeRawData = value;
        }
    }
}
