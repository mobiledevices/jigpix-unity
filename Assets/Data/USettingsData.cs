﻿using UnityEngine;

public class USettingsData
{
    private const string cSaveOriginalPhotosKey = "SaveOriginalPhotos";
    private const string cAutoSavePuzzleArtwork = "AutoSavePuzzleArtwork";

    private bool m_SaveOriginalPhotos = true;
    private bool m_AutoSavePuzzleArtwork = false;

    public bool upSaveOriginalPhotos
    {
        get
        {
            return m_SaveOriginalPhotos;
        }
        set
        {
            m_SaveOriginalPhotos = value;
            PlayerPrefs.SetInt(cSaveOriginalPhotosKey, m_SaveOriginalPhotos ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    public bool upAutoSavePuzzleArtwork
    {
        get
        {
            return m_AutoSavePuzzleArtwork;
        }
        set
        {
            m_AutoSavePuzzleArtwork = value;
            PlayerPrefs.SetInt(cAutoSavePuzzleArtwork, m_AutoSavePuzzleArtwork ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    public USettingsData()
    {
        if (PlayerPrefs.HasKey(cSaveOriginalPhotosKey))
        {
            m_SaveOriginalPhotos = PlayerPrefs.GetInt(cSaveOriginalPhotosKey) == 1;
        }
        if (PlayerPrefs.HasKey(cAutoSavePuzzleArtwork))
        {
            m_AutoSavePuzzleArtwork = PlayerPrefs.GetInt(cAutoSavePuzzleArtwork) == 1;
        }
    }
}