﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public class XCodeSettingsPostProcesser
{
    [PostProcessBuildAttribute(0)]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string pathToBuiltProject)
    {

        // Stop processing if targe is NOT iOS
        if (buildTarget != BuildTarget.iOS)
            return;

        // Initialize PbxProject
        var projectPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
        PBXProject pbxProject = new PBXProject();
        pbxProject.ReadFromFile(projectPath);
        string targetGuid = pbxProject.TargetGuidByName("Unity-iPhone");

        // Sample of adding build property
        //pbxProject.AddBuildProperty(targetGuid, "OTHER_LDFLAGS", "-lz");

        //// Sample of setting build property
        //pbxProject.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");

        //// Sample of update build property
        //pbxProject.UpdateBuildProperty(targetGuid, "OTHER_LDFLAGS", new string[] { "-ObjC" }, new string[] { "-weak_framework" });

        //// Sample of adding REQUIRED framwrok
        pbxProject.AddFrameworkToProject(targetGuid, "Accounts.framework", false);
        pbxProject.AddFrameworkToProject(targetGuid, "Social.framework", false);
        pbxProject.AddFrameworkToProject(targetGuid, "MessageUI.framework", false);
        pbxProject.AddFrameworkToProject(targetGuid, "MobileCoreServices.framework", false);

        //// Sample of adding OPTIONAL framework
        //pbxProject.AddFrameworkToProject(targetGuid, "SafariServices.framework", true);

        //// Sample of setting compile flags
        //var guid = pbxProject.FindFileGuidByProjectPath("Classes/UI/Keyboard.mm");
        //var flags = pbxProject.GetCompileFlagsForFile(targetGuid, guid);
        //flags.Add("-fno-objc-arc");
        //pbxProject.SetCompileFlagsForFile(targetGuid, guid, flags);

        // Apply settings
        File.WriteAllText(projectPath, pbxProject.WriteToString());

        //// Samlpe of editing Info.plist
        //var plistPath = Path.Combine(pathToBuiltProject, "Info.plist");
        //var plist = new PlistDocument();
        //plist.ReadFromFile(plistPath);

        //// Add string setting
        //plist.root.SetString("hogehogeId", "dummyid");

        //// Add URL Scheme
        //var array = plist.root.CreateArray("CFBundleURLTypes");
        //var urlDict = array.AddDict();
        //urlDict.SetString("CFBundleURLName", "hogehogeName");
        //var urlInnerArray = urlDict.CreateArray("CFBundleURLSchemes");
        //urlInnerArray.AddString("hogehogeValue");

        //// Apply editing settings to Info.plist
        //plist.WriteToFile(plistPath);

        UpdatePreprocessor(pathToBuiltProject);

        UpdateInfoPlist(pathToBuiltProject);
    }

    private static void UpdatePreprocessor(string pathToBuiltProject)
    {
        string preprocessorPath = pathToBuiltProject + "/Classes/Preprocessor.h";
        string text = File.ReadAllText(preprocessorPath);
        text = text.Replace("UNITY_USES_REMOTE_NOTIFICATIONS 1", "UNITY_USES_REMOTE_NOTIFICATIONS 0");
        File.WriteAllText(preprocessorPath, text);
    }

    private static void UpdateInfoPlist(string path)
    {
        //Get Plist
        string plistPath = Path.Combine(path, "Info.plist");
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        //Get Root
        PlistElementDict rootDict = plist.root;

        //Add Necessary Things
        PlistElementArray LSApplicationQueriesSchemes = rootDict.CreateArray("LSApplicationQueriesSchemes");
        LSApplicationQueriesSchemes.AddString("fb");
        LSApplicationQueriesSchemes.AddString("instagram");

        // Add photo library usage description for iOS 10
        rootDict.SetString("NSPhotoLibraryUsageDescription", "for photos usage");

        //WriteFile
        File.WriteAllText(plistPath, plist.WriteToString());
    }
}
